      subroutine idspstt(d,z,ttp,tkfp,slwp,tts,tkfs,slws)
      use idsalloc
      implicit none
      real*8 d,z,ttp,tkfp,slwp,tts,tkfs,slws
c
      integer*4 ig,iz1,iz2,ir,ir1,ir2
      real*8 z1,z2,dzps,d1,d2,ddps
c
      if(d.lt.2.d0*r(1)-r(2).or.d.gt.2.d0*r(nr)-r(nr-1))then
        stop 'Error in idspstt: distance not within'
     &   //' the Green function coverage!'
      else if(z.lt.2.d0*deps(1)-deps(2).or.
     &        z.gt.2.d0*deps(ng)-deps(ng-1))then
        stop 'Error in idspstt: depth not within'
     &   //' the Green function coverage!'
      endif
c
      if(z.le.deps(1))then
        iz1=1
        iz2=2
      else if(z.ge.deps(ng))then
        iz1=ng-1
        iz2=ng
      else
        iz2=0
        do ig=1,ng
          if(z.le.deps(ig))then
            iz2=ig
            goto 100
          endif
        enddo
100     continue
        iz1=iz2-1
      endif
c
      z1=deps(iz1)
      z2=deps(iz2)
      dzps=z2-z1
c
      if(d.le.r(1))then
        ir1=1
        ir2=2
      else if(d.ge.r(nr))then
        ir1=nr-1
        ir2=nr
      else
        do ir=1,nr
          if(d.le.r(ir))then
            ir2=ir
            goto 200
          endif
        enddo
200     continue
        ir1=ir2-1
      endif
c
      d1=r(ir1)
      d2=r(ir2)
      ddps=d2-d1
c
      ttp=((d2-d)*(z-z1)*dble(tp(ir1,iz2))
     &    +(d-d1)*(z-z1)*dble(tp(ir2,iz2))
     &    +(d2-d)*(z2-z)*dble(tp(ir1,iz1))
     &    +(d-d1)*(z2-z)*dble(tp(ir2,iz1)))/(dzps*ddps)
c
      tkfp=((d2-d)*(z-z1)*dble(tkftp(ir1,iz2))
     &     +(d-d1)*(z-z1)*dble(tkftp(ir2,iz2))
     &     +(d2-d)*(z2-z)*dble(tkftp(ir1,iz1))
     &     +(d-d1)*(z2-z)*dble(tkftp(ir2,iz1)))/(dzps*ddps)
c
      slwp=((d2-d)*(z-z1)*dble(slwtp(ir1,iz2))
     &     +(d-d1)*(z-z1)*dble(slwtp(ir2,iz2))
     &     +(d2-d)*(z2-z)*dble(slwtp(ir1,iz1))
     &     +(d-d1)*(z2-z)*dble(slwtp(ir2,iz1)))/(dzps*ddps)
c
      tts=((d2-d)*(z-z1)*dble(ts(ir1,iz2))
     &    +(d-d1)*(z-z1)*dble(ts(ir2,iz2))
     &    +(d2-d)*(z2-z)*dble(ts(ir1,iz1))
     &    +(d-d1)*(z2-z)*dble(ts(ir2,iz1)))/(dzps*ddps)
c
      tkfs=((d2-d)*(z-z1)*dble(tkfts(ir1,iz2))
     &     +(d-d1)*(z-z1)*dble(tkfts(ir2,iz2))
     &     +(d2-d)*(z2-z)*dble(tkfts(ir1,iz1))
     &     +(d-d1)*(z2-z)*dble(tkfts(ir2,iz1)))/(dzps*ddps)
c
      slws=((d2-d)*(z-z1)*dble(slwts(ir1,iz2))
     &     +(d-d1)*(z-z1)*dble(slwts(ir2,iz2))
     &     +(d2-d)*(z2-z)*dble(slwts(ir1,iz1))
     &     +(d-d1)*(z2-z)*dble(slwts(ir2,iz1)))/(dzps*ddps)
      return
      end