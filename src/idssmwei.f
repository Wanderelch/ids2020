      subroutine idssmwei(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,j,k,m,ism,jsm
      real*8 corr,summa,tpi,tpj
      complex*16 ca,cb
c
      allocate (cswap(2*nt,max0(6,nsm)),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsmwei: cswap not allocated!'
      allocate (dswap(6*nt,max0(6,nsm)),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmwei: dswap not allocated!'
c
      do ism=1,nsm
        summa=0.d0
        do j=1,3
          do i=1,nf
            ca=cdexp(dcmplx(0.d0,
     &        omi(i)*(tpsmgrn(isfhyp,ism)-t0sm(ism)-TPRE)))
            cfct(i)=smgrn(i,j,isfhyp,ism)*ca
          enddo
          m=1
          do i=2*nf,nf+2,-1
            m=m+1
            cfct(i)=dconjg(cfct(m))
          enddo
          cfct(nf+1)=(0.d0,0.d0)
          call four1w(cfct,dfct,2*nf,+1)
          k=(j-1)*nt
          do i=1,nt
            dswap(k+i,ism)=smwei0(j,ism)*dfct(2*i-1)
            summa=summa+dswap(k+i,ism)**2
          enddo
        enddo
        if(summa.gt.0.d0)then
          summa=dsqrt(summa)
          do i=1,3*nt
            dswap(i,ism)=dswap(i,ism)/summa
          enddo
        endif
      enddo
c
      do ism=1,nsm
        tpi=tpsmgrn(isfhyp,ism)
        corr=1.d0
        do jsm=1,nsm
          if(jsm.ne.ism)then
            tpj=tpsmgrn(isfhyp,jsm)
            summa=0.d0
            if(dabs(tpi-tpj).lt.dble(nt-1)*dt)then
              m=idint((tpi-tpj)/dt)
              do j=1,3
                k=(j-1)*nt
                if(m.ge.0)then
                  do i=1,nt-m
                    summa=summa+dswap(k+i,ism)*dswap(k+i+m,jsm)
                  enddo
                else
                  m=-m
                  do i=1,nt-m
                    summa=summa+dswap(k+i+m,ism)*dswap(k+i,jsm)
                  enddo
                endif
              enddo
              if(summa.gt.0.d0)corr=corr+summa
            endif
          endif
        enddo
        do j=1,3
          smwei0(j,ism)=smwei0(j,ism)/corr
        enddo
      enddo
c
c     normalize and modify weighting depending on P arrival deviation
c
      summa=0.d0
      do ism=1,nsm
        do j=1,3
          if(smwei0(j,ism).le.0.d0)then
            smwei0(j,ism)=0.d0
          else
            summa=summa+smwei0(j,ism)
          endif
        enddo
      enddo
      do ism=1,nsm
        do j=1,3
          if(smwei0(j,ism).gt.0.d0)smwei0(j,ism)=smwei0(j,ism)/summa
        enddo
      enddo
c
      return
      end