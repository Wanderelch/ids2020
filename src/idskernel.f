      subroutine idskernel(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,j,k,m,isf,jsf,ksf,ism,igns,isar
      integer*4 it1,it2,jt1,jt2,itc,iter,jter,isfc
      real*8 dslp,slpm,roughness,fradius,farea85,farea95,farea99
      real*8 a,b,a2,b2,c2,ab,t1,suma2,sumab,pkslp,maxslp
      real*8 alpha,delta,gamma,sigma,dism
      real*8 smscl,ttscl,allwei
      real*8 nrv0,nrv,smnrv0,smnrv,osmnrv0,osmnrv
      real*8 gnsnrv0,gnsnrv,sarnrv0,sarnrv
      complex*16 ca,cb
      logical*2 hvce
c
c      real*8 water
c      data water/0.10d+00/
c
c     initialization
c
      open(30,file=logiteration,status='unknown')
      write(*,'(a)')' ******************************************'
      write(*,'(a)')'               IDS iterations              '
      write(*,'(a)')' ******************************************'
      write(*,'(a)')' (nrv = normalized residual variance)'
      write(*,'(a)')' (swf = seism. waveform, osm = disp. offset from'
     &         //' SM, gns = disp. from GNSS, sar = disp. from InSar)'
      write(*,'(a,$)')' iteration     nrv_all     nrv_swf'
      if(osmsetwei.gt.0.d0)write(*,'(a,$)')'     nrv_osm'
      if(ngns.gt.0)write(*,'(a,$)')'     nrv_gns'
      if(nsar.gt.0)write(*,'(a,$)')'     nrv_sar'
      write(*,'(a)')' smoothing  max_slip  patch_no        Mw'
c
      write(30,'(a)')' ******************************************'
      write(30,'(a)')'               IDS iterations              '
      write(30,'(a)')' ******************************************'
      write(30,'(a)')' (nrv = normalized residual variance)'
      write(30,'(a)')' (swf = seism. waveform, osm = disp. offset from'
     &         //' SM, gns = disp. from GNSS, sar = disp. from InSar)'
      write(30,'(a,$)')' iteration     nrv_all     nrv_swf'
      if(osmsetwei.gt.0.d0)write(30,'(a,$)')'     nrv_osm'
      if(ngns.gt.0)write(30,'(a,$)')'     nrv_gns'
      if(nsar.gt.0)write(30,'(a,$)')'     nrv_sar'
      write(30,'(a)')' smoothing  max_slip  patch_no        Mw'
c
      allocate(pkspec(3,nsf,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idskernel: pkspec not allocated!'
      allocate(fswap(2*nf+nsf),stat=ierr)
      if(ierr.ne.0)stop ' Error in idskernel: fswap not allocated!'
      allocate(gswap(2*nf+nsf),stat=ierr)
      if(ierr.ne.0)stop ' Error in idskernel: gswap not allocated!'
      allocate(clust(nsf),stat=ierr)
      if(ierr.ne.0)stop ' Error in idskernel: clust not allocated!'
      allocate(damping(2*nf),stat=ierr)
      if(ierr.ne.0)stop ' Error in idskernel: damping not allocated!'
c
c     initialization
c
      allwei=1.d0+osmsetwei+gnssetwei+sarsetwei
c
      hvce=.true.
c
      do isf=1,nsf
        do i=1,nf
          sstf(i,isf)=(0.d0,0.d0)
        enddo
      enddo
c
      do ism=1,nsm
        do j=1,3
          smvar(j,ism)=0.d0
          do i=1,nf
            dsmobs(i,j,ism)=ssmobs(i,j,ism)
            ssmsyn(i,j,ism)=(0.d0,0.d0)
            dsmsyn(i,j,ism)=(0.d0,0.d0)
            smvar(j,ism)=smvar(j,ism)+cdabs(ssmobs(i,j,ism))**2
          enddo
          dosmobs(j,ism)=sosmobs(j,ism)
          sosmsyn(j,ism)=0.d0
          dosmsyn(j,ism)=0.d0
        enddo
      enddo
c
c     define water level parameters
c
c      do ism=1,nsm
c        do isf=1,nsf
c          do j=1,3
c            pkspec(j,isf,ism)=0.d0
c            do i=1,nf
c              pkspec(j,isf,ism)=dmax1(pkspec(j,isf,ism),
c     &                          cdabs(smgrn(i,j,isf,ism))**2)
c            enddo
c          enddo
c        enddo
c      enddo
c
      if(ngns.gt.0)then
        sigma=0.d0
        do igns=1,ngns
          do j=1,3
            dgnsobs(j,igns)=sgnsobs(j,igns)
            sgnssyn(j,igns)=0.d0
            dgnssyn(j,igns)=0.d0
            sigma=sigma+sgnsobs(j,igns)**2*gnswei(j,igns)
          enddo
        enddo
        do igns=1,ngns
          do j=1,3
            gnswei(j,igns)=gnswei(j,igns)/sigma
          enddo
        enddo
      endif
c
      if(nsar.gt.0)then
        sigma=0.d0
        do isar=1,nsar
          dsarobs(isar)=ssarobs(isar)
          ssarsyn(isar)=0.d0
          dsarsyn(isar)=0.d0
          sigma=sigma+ssarobs(isar)**2*sarwei(isar)
        enddo
        do isar=1,nsar
          sarwei(isar)=sarwei(isar)/sigma
        enddo
      endif
c
      nrv=1.d0
      smnrv=1.d0
      gnsnrv=1.d0
      sarnrv=1.d0
c
      iter=0
      fradius=dsqrt(dble(nsf)*patchsize**2/PI)
      roughness=0.d0
      maxslp=0.d0
      do isf=1,nsf
        clust(isf)=1.d0
      enddo
c
      it1=1+idint(trise/dt)
      it2=1+idint(swindow/dt)
      do i=1,it1
        damping(i)=1.d0
      enddo
      do i=1+it1,it2
        damping(i)=dcos(0.5d0*PI*dble(i-it1)/dble(it2-it1))**2
      enddo
      do i=1+it2,2*nf
        damping(i)=0.d0
      enddo
c
      itmax=0
c
100   iter=iter+1
c
c     update normalized weighting factors using the hvce approach
c
      do ism=1,nsm
        do j=1,3
          dsmvar(j,ism)=0.d0
          do i=1,nf
            dsmvar(j,ism)=dsmvar(j,ism)+cdabs(dsmobs(i,j,ism))**2
          enddo
          dsmvar(j,ism)=(1.d0-SDWEI)*dsmvar(j,ism)+SDWEI*smvar(j,ism)
          if(dsmvar(j,ism).gt.0.d0)then
            smwei(j,ism)=smwei0(j,ism)/dsmvar(j,ism)
          else
            smwei(j,ism)=0.d0
          endif
        enddo
      enddo
c
      gamma=0.d0
      do ism=1,nsm
        do j=1,3
          gamma=gamma+smwei(j,ism)*smvar(j,ism)
        enddo
      enddo
c
      do ism=1,nsm
        do j=1,3
          smwei(j,ism)=smwei(j,ism)/gamma
        enddo
      enddo
c
      do ism=1,nsm
        if(nfpsv(ism).eq.0)then
          do j=1,3
            if(dsmvar(j,ism).gt.0.d0)then
              osmwei(j,ism)=smwei0(j,ism)*smvar(j,ism)/dsmvar(j,ism)
            else
              osmwei(j,ism)=0.d0
            endif
          enddo
        else
          do j=1,3
            osmwei(j,ism)=0.d0
          enddo
        endif
      enddo
c
      sigma=0.d0
      do ism=1,nsm
        do j=1,3
          sigma=sigma+sosmobs(j,ism)**2*osmwei(j,ism)
        enddo
      enddo
c
      if(sigma.gt.0.d0)then
        do ism=1,nsm
          do j=1,3
            osmwei(j,ism)=osmwei(j,ism)/sigma
          enddo
        enddo
      endif
c
      smnrv0=0.d0
      do ism=1,nsm
        do j=1,3
          if(smwei(j,ism).gt.0.d0)then
            a2=0.d0
            do i=1,nf
              a2=a2+cdabs(dsmobs(i,j,ism))**2
            enddo
            smnrv0=smnrv0+a2*smwei(j,ism)
          endif
        enddo
      enddo
      osmnrv0=0.d0
      do ism=1,nsm
        do j=1,3
          osmnrv0=osmnrv0+dosmobs(j,ism)**2*osmwei(j,ism)
        enddo
      enddo
      gnsnrv0=0.d0
      do igns=1,ngns
        do j=1,3
          gnsnrv0=gnsnrv0+dgnsobs(j,igns)**2*gnswei(j,igns)
        enddo
      enddo
      sarnrv0=0.d0
      do isar=1,nsar
        sarnrv0=sarnrv0+dsarobs(isar)**2*sarwei(isar)
      enddo
      nrv0=(smnrv0+osmnrv0*osmsetwei
     &     +gnsnrv0*gnssetwei+sarnrv0*sarsetwei)/allwei
c
      pkslp=0.d0
      do isf=1,nsf
        do i=1,nf
          dstf(i,isf)=(0.d0,0.d0)
        enddo
c
        if(it1sf(isf).ge.it2sf(isf).or.clust(isf).le.0.d0)goto 600
c
        if(iter.gt.1)then
          delta=sfslp(isf)
          do j=1,nsfnb(isf)
            jsf=isfnb(j,isf)
            delta=delta+sfslp(jsf)
          enddo
          if(delta.le.0.d0)goto 600
        endif
c
        do i=1,nf
          cfct(i)=(0.d0,0.d0)
        enddo
c
        do ism=1,nsm
          do j=1,3
c
c           deconvolution
c
            if(smwei0(j,ism).gt.0.d0)then
              alpha=0.d0
              delta=0.d0
              do i=1,nf
                cswap(i,j)=dsmobs(i,j,ism)*dconjg(smgrn(i,j,isf,ism))
     &                    *dcmplx(omi(i)**2,0.d0)
c     &             /dcmplx(dmax1(cdabs(smgrn(i,j,isf,ism))**2,
c     &                           water*pkspec(j,isf,ism)),0.d0)
                alpha=alpha+(omi(i)*cdabs(smgrn(i,j,isf,ism)))**2
                delta=delta+(omi(i)*cdabs(dsmobs(i,j,ism)*omi(i)))**2
              enddo
              sigma=smwei(j,ism)*smvar(j,ism)/dsqrt(alpha*delta)
c
c             stacking
c
              do i=1,nf
                cfct(i)=cfct(i)+cswap(i,j)*dcmplx(sigma,0.d0)
              enddo
            endif
          enddo
        enddo
c
c       end estimating STF of the isf-th target sub-fault
c
        m=1
        do i=2*nf,nf+2,-1
          m=m+1
          cfct(i)=dconjg(cfct(m))
        enddo
        cfct(nf+1)=(0.d0,0.d0)
c
        call four1w(cfct,dfct,2*nf,+1)
c
        do i=1,2*nf
          fswap(i)=dmax1(dfct(2*i-1),0.d0)
        enddo
c
c       search max. positive wavelet of the apparent source time function
c
300     continue
c
        it1=2*nf
        do i=it1sf(isf),it2sf(isf)
          if(fswap(i).le.0.d0)then
            it1=i
            goto 311
          endif
        enddo
311     continue
        it2=1
        do i=it2sf(isf),2*nf
          if(fswap(i).le.0.d0)then
            it2=i
            goto 312
          endif
        enddo
312     continue
c
        itc=0
        delta=0.d0
        do i=it1,it2
          if(fswap(i)*damping(i).gt.delta)then
            itc=i
            delta=fswap(i)*damping(i)
          endif
        enddo
c
        if(delta.le.0.d0)goto 600
c
        it1=it1sf(isf)
        do i=itc-1,1,-1
          if(fswap(i).le.0.d0)then
            it1=i
            goto 321
          endif
        enddo
321     continue
c
        it2=it2sf(isf)
        do i=itc+1,2*nf
          if(fswap(i).le.0.d0)then
            it2=i
            goto 322
          endif
        enddo
322     continue
c
        do i=1,2*nf
          cfct(i)=(0.d0,0.d0)
        enddo
        do i=it1,it2
          cfct(i)=dcmplx(fswap(i),0.d0)
        enddo
c
        call four1w(cfct,dfct,2*nf,-1)
c
        do i=1,nf
          cfct(i)=cfct(i)*lpfs(i)
        enddo
c
        dslp=dreal(cfct(1))
c
c       correlation between synthetics of selected STF and residual data
c
        suma2=0.d0
        sumab=0.d0
        do ism=1,nsm
          do j=1,3
            if(smwei(j,ism).gt.0.d0)then
              a2=0.d0
              ab=0.d0
              do i=1,nf
                ca=smgrn(i,j,isf,ism)*cfct(i)
                cb=dsmobs(i,j,ism)
                a2=a2+cdabs(ca)**2
                ab=ab+dreal(ca*dconjg(cb))
              enddo
              suma2=suma2+a2*smwei(j,ism)
              sumab=sumab+ab*smwei(j,ism)
            endif
          enddo
        enddo
c
        do ism=1,nsm
          do j=1,3
            suma2=suma2+(dslp*osmgrn(j,isf,ism))**2
     &                 *osmwei(j,ism)*osmsetwei
            sumab=sumab+dslp*osmgrn(j,isf,ism)*dosmobs(j,ism)
     &                 *osmwei(j,ism)*osmsetwei
          enddo
        enddo
c
        do igns=1,ngns
          do j=1,3
            suma2=suma2+(dslp*gnsgrn(j,isf,igns))**2
     &                 *gnswei(j,igns)*gnssetwei
            sumab=sumab+dslp*gnsgrn(j,isf,igns)*dgnsobs(j,igns)
     &                 *gnswei(j,igns)*gnssetwei
          enddo
        enddo
c
        do isar=1,nsar
          suma2=suma2+(dslp*sargrn(isf,isar))**2
     &               *sarwei(isar)*sarsetwei
          sumab=sumab+dslp*sargrn(isf,isar)*dsarobs(isar)
     &               *sarwei(isar)*sarsetwei
        enddo
c
        if(sumab.gt.0.d0)then
          ttscl=sumab/suma2
          do i=1,nf
            cfct(i)=cfct(i)*dcmplx(ttscl,0.d0)
          enddo
          dslp=dslp*ttscl
          suma2=suma2*ttscl**2
        else
          do i=it1,it2
            fswap(i)=0.d0
          enddo
          goto 300
        endif
c
c       sensitivity factor (reduction of misfit variance to original data)
c
        sumab=0.d0
        do ism=1,nsm
          do j=1,3
            if(smwei(j,ism).gt.0.d0)then
              ab=0.d0
              do i=1,nf
                ca=smgrn(i,j,isf,ism)*cfct(i)
                cb=ssmobs(i,j,ism)
                ab=ab+dreal(ca*dconjg(cb))
              enddo
              sumab=sumab+ab*smwei(j,ism)
            endif
          enddo
        enddo
c
        do ism=1,nsm
          do j=1,3
            sumab=sumab+dslp*osmgrn(j,isf,ism)*sosmobs(j,ism)
     &                 *osmwei(j,ism)*osmsetwei
          enddo
        enddo
c
        do igns=1,ngns
          do j=1,3
            sumab=sumab+dslp*gnsgrn(j,isf,igns)*sgnsobs(j,igns)
     &                 *gnswei(j,igns)*gnssetwei
          enddo
        enddo
c
        do isar=1,nsar
          sumab=sumab+dslp*sargrn(isf,isar)*ssarobs(isar)
     &               *sarwei(isar)*sarsetwei
        enddo
c
        if(sumab.gt.0.d0)then
          alpha=dmin1(1.d0,sumab/suma2)
          sigma=alpha*(2.d0*sumab-alpha*suma2)*clust(isf)
          do i=1,nf
            dstf(i,isf)=cfct(i)*dcmplx(sigma,0.d0)
          enddo
          sfslp(isf)=dreal(dstf(1,isf))
          pkslp=dmax1(pkslp,sfslp(isf))
          it1sf(isf)=min0(it1sf(isf),it1)
          itmax=max0(itmax,it2)
        else
          do i=it1,it2
            fswap(i)=0.d0
          enddo
          goto 300
        endif
600     continue
      enddo
c
      if(pkslp.le.0.d0)then
        iter=iter-1
        write(*,'(a)')' iteration terminated: no more subfault signal.'
        write(30,'(a)')' iteration terminated: no more subfault signal.'
        goto 900
      endif
c
      if(iter.eq.1)then
        call idsscale(1,smscl,ttscl)
        if(ttscl.le.0.d0)then
          stop ' Error in idskernel: no subevent found!'
        endif
        do isf=1,nsf
          do i=1,nf
            dstf(i,isf)=dstf(i,isf)*dcmplx(ttscl,0.d0)
          enddo
          sfslp(isf)=dreal(dstf(1,isf))
        enddo
c
c       estimate reference roughness after the 1. iteration
c
        slpm=0.d0
        do isf=1,nsf
          slpm=slpm+sfslp(isf)**2
        enddo
        slpm=dsqrt(slpm)/dble(nsf)
c
        sigma=0.d0
        do isf=1,nsf
          delta=sfslp(isf)
          do j=1,nsfnb(isf)
            delta=delta+sfslp(isfnb(j,isf))
          enddo
          delta=delta/dble(1+nsfnb(isf))
          sigma=sigma+(sfslp(isf)-delta)**2
        enddo
        roughness=dsqrt(sigma)/slpm
        jter=0
      else
c
c       eatimate slip roughness and start smoothing
c
        jter=0
700     jter=jter+1
c
        slpm=0.d0
        do isf=1,nsf
          sfslp(isf)=dreal(dstf(1,isf))
          slpm=slpm+sfslp(isf)**2
        enddo
        slpm=dsqrt(slpm)/dble(nsf)
c
        sigma=0.d0
        do isf=1,nsf
          delta=sfslp(isf)
          do j=1,nsfnb(isf)
            delta=delta+sfslp(isfnb(j,isf))
          enddo
          delta=delta/dble(1+nsfnb(isf))
          sigma=sigma+(sfslp(isf)-delta)**2
        enddo
        sigma=dsqrt(sigma)/slpm
c
        if(sigma.gt.roughness)then
          do isf=1,nsf
            do i=1,nf
              stfswap(i,isf)=dstf(i,isf)
            enddo
          enddo
c
          do isf=1,nsf
            do j=1,nsfnb(isf)
              jsf=isfnb(j,isf)
              do i=1,nf
                dstf(i,isf)=dstf(i,isf)+stfswap(i,jsf)
              enddo
            enddo
            do i=1,nf
              dstf(i,isf)=dstf(i,isf)/dcmplx(dble(1+nsfnb(isf)),0.d0)
            enddo
          enddo
          goto 700
        endif
c
        call idsscale(1,smscl,ttscl)
c
        if(ttscl.le.0.d0)then
          iter=iter-1
          write(*,'(a)')' iteration terminated: no more subevent found.'
          write(30,'(a)')' iteration terminated: '
     &                 //'no more subevent found.'
          goto 900
        endif
        do isf=1,nsf
          do i=1,nf
            dstf(i,isf)=dstf(i,isf)*dcmplx(ttscl,0.d0)
          enddo
        enddo
      endif
c
      if(iter.eq.1.or.smscl.le.0.d0)then
        ca=(0.5d0,0.d0)
      else if(smscl.ge.ttscl)then
        ca=(1.d0,0.d0)
      else
        ca=dcmplx(smscl/ttscl,0.d0)
      endif
c
      do isf=1,nsf
        sfswap(isf)=dreal(sstf(1,isf)+ca*dstf(1,isf))
        if(sfswap(isf).gt.0.d0)then
          do i=1,nf
            sstf(i,isf)=sstf(i,isf)+ca*dstf(i,isf)
          enddo
        else
          sfswap(isf)=0.d0
          do i=1,nf
            sstf(i,isf)=(0.d0,0.d0)
          enddo
        endif
      enddo
      if(hvce)then
c
c       estimate fault size
c
        sigma=0.d0
        do isf=1,nsf
          fswap(isf)=sfswap(isf)
          gswap(isf)=sflen(isf)*sfwid(isf)
          sigma=sigma+fswap(isf)*gswap(isf)
        enddo
c
        gamma=0.d0
        farea85=0.d0
        farea95=0.d0
        farea99=0.d0
        do isf=1,nsf
          do jsf=isf+1,nsf
            if(fswap(jsf).gt.fswap(isf))then
              a=fswap(isf)
              fswap(isf)=fswap(jsf)
              fswap(jsf)=a
              b=gswap(isf)
              gswap(isf)=gswap(jsf)
              gswap(jsf)=b
            endif
          enddo
          gamma=gamma+fswap(isf)*gswap(isf)
          if(gamma.le.0.85d0*sigma)then
            farea85=farea85+gswap(isf)
          endif
          if(gamma.le.0.95d0*sigma)then
            farea95=farea95+gswap(isf)
          endif
          if(gamma.le.0.99d0*sigma)then
            farea99=farea99+gswap(isf)
          endif
        enddo
c
        fradius=dsqrt(farea85/PI)
c
c       update clustering factor
c
        do isf=1,nsf
          clust(isf)=0.d0
          do jsf=1,nsf
            clust(isf)=clust(isf)+sfswap(isf)*sflen(jsf)*sfwid(jsf)
     &                /(dis3dsf(isf,jsf)+fradius)
          enddo
        enddo
c
        do isf=1,nsf
          fswap(isf)=clust(isf)
          gswap(isf)=sflen(isf)*sfwid(isf)
        enddo
c
        delta=0.d0
        do isf=1,nsf
          do jsf=isf+1,nsf
            if(fswap(jsf).gt.fswap(isf))then
              a=fswap(isf)
              fswap(isf)=fswap(jsf)
              fswap(jsf)=a
              b=gswap(isf)
              gswap(isf)=gswap(jsf)
              gswap(jsf)=b
            endif
          enddo
          delta=delta+gswap(isf)
          if(delta.le.farea95)then
            alpha=fswap(isf)
          endif
          if(delta.le.farea99)then
            gamma=fswap(isf)
          endif
        enddo
c
        do isf=1,nsf
          clust(isf)=dmax1(0.d0,clust(isf)-gamma)
        enddo
        alpha=alpha-gamma
        do isf=1,nsf
          clust(isf)=dmin1(1.d0,clust(isf)/alpha)
        enddo
c
        if(iter.eq.1)then
          do isf=1,nsf
            do i=1,nf
              sstf(i,isf)=sstf(i,isf)*dcmplx(clust(isf),0.d0)
            enddo
          enddo
        endif
c
c       suppress peak slips
c
        do isf=1,nsf
          delta=0.d0
          do j=1,nsfnb(isf)
            delta=delta+sfslp(isfnb(j,isf))
          enddo
          delta=delta/dble(nsfnb(isf))
          if(sfslp(isf).gt.delta)then
            clust(isf)=clust(isf)*(delta/sfslp(isf))**2
          endif
        enddo
      endif
c
c     re-scaling
c
      call idsscale(0,smscl,ttscl)
c
      if(ttscl.le.0.d0)then
        iter=iter-1
        write(*,'(a)')' iteration terminated: abnormal scaling result.'
        write(30,'(a)')' iteration terminated: abnormal scaling result.'
        goto 900
      endif
c
      ttscl=dmin1(1.d0,ttscl)
c
      do isf=1,nsf
        do i=1,nf
          sstf(i,isf)=sstf(i,isf)*dcmplx(ttscl,0.d0)
        enddo
      enddo
c
      do ism=1,nsm
        do j=1,3
          do i=1,nf
            ssmsyn(i,j,ism)=ssmsyn(i,j,ism)*dcmplx(ttscl,0.d0)
            dsmobs(i,j,ism)=ssmobs(i,j,ism)-ssmsyn(i,j,ism)
          enddo
        enddo
      enddo
c
      do ism=1,nsm
        do j=1,3
          sosmsyn(j,ism)=sosmsyn(j,ism)*ttscl
          dosmobs(j,ism)=sosmobs(j,ism)-sosmsyn(j,ism)
        enddo
      enddo
c
      do igns=1,ngns
        do j=1,3
          sgnssyn(j,igns)=sgnssyn(j,igns)*ttscl
          dgnsobs(j,igns)=sgnsobs(j,igns)-sgnssyn(j,igns)
        enddo
      enddo
c
      do isar=1,nsar
        ssarsyn(isar)=ssarsyn(isar)*ttscl
        dsarobs(isar)=ssarobs(isar)-ssarsyn(isar)
      enddo
c
      maxslp=0.d0
      do i=1,nf
        cfct(i)=(0.d0,0.d0)
      enddo
c
      mw=0.d0
      isfc=0
      do isf=1,nsf
        gamma=sflen(isf)*sfwid(isf)*sfmue(isf)
        sfslp(isf)=dreal(sstf(1,isf))
        mw=mw+sfslp(isf)*gamma
        if(maxslp.lt.sfslp(isf))then
          maxslp=sfslp(isf)
          isfc=isf
        endif
      enddo
c
      mw=(dlog10(mw)-9.1d0)/1.5d0
c
      smnrv=0.d0
      do ism=1,nsm
        do j=1,3
          if(smwei(j,ism).gt.0.d0)then
            a2=0.d0
            do i=1,nf
              a2=a2+cdabs(dsmobs(i,j,ism))**2
            enddo
            smnrv=smnrv+a2*smwei(j,ism)
          endif
        enddo
      enddo
      osmnrv=0.d0
      do ism=1,nsm
        do j=1,3
          osmnrv=osmnrv+dosmobs(j,ism)**2*osmwei(j,ism)
        enddo
      enddo
      gnsnrv=0.d0
      do igns=1,ngns
        do j=1,3
          gnsnrv=gnsnrv+dgnsobs(j,igns)**2*gnswei(j,igns)
        enddo
      enddo
      sarnrv=0.d0
      do isar=1,nsar
        sarnrv=sarnrv+dsarobs(isar)**2*sarwei(isar)
      enddo
      nrv=(smnrv+osmnrv*osmsetwei
     &    +gnsnrv*gnssetwei+sarnrv*sarsetwei)/allwei
c
      write(*,'(i10,2f12.6,$)')iter,nrv,smnrv
      if(osmsetwei.gt.0.d0)write(*,'(f12.6,$)')osmnrv
      if(ngns.gt.0)write(*,'(f12.6,$)')gnsnrv
      if(nsar.gt.0)write(*,'(f12.6,$)')sarnrv
      write(*,'(i10,f10.4,i10,f10.4)')jter,maxslp,isfc,mw
c
      write(30,'(i10,2f12.6,$)')iter,nrv,smnrv
      if(osmsetwei.gt.0.d0)write(30,'(f12.6,$)')osmnrv
      if(ngns.gt.0)write(30,'(f12.6,$)')gnsnrv
      if(nsar.gt.0)write(30,'(f12.6,$)')sarnrv
      write(30,'(i10,f10.4,i10,f10.4)')jter,maxslp,isfc,mw
c
      if(iter.eq.itermax)then
        write(*,'(a)')' iteration terminated: max. iterations reached.'
        write(30,'(a)')' iteration terminated: max. iterations reached.'
      else if(nrv0-nrv.lt.1.0d-03*nrv.and.iter.gt.20)then
        write(*,'(a)')' iteration terminated: convergence.'
        write(30,'(a)')' iteration terminated: convergence.'
      else
        goto 100
      endif
c
900   continue
c
      suma2=0.d0
      sumab=0.d0
      do ism=1,nsm
        do j=1,3
          if(smwei(j,ism).gt.0.d0)then
            sigma=0.d0
            gamma=0.d0
            do i=1,nf
              sigma=sigma+cdabs(ssmsyn(i,j,ism))**2
              gamma=gamma+cdabs(ssmobs(i,j,ism))**2
            enddo
            suma2=suma2+sigma**2*smwei(j,ism)
            sumab=sumab+sigma*gamma*smwei(j,ism)
          endif
        enddo
      enddo
      ttscl=sumab/suma2
      mwsm=mw+dlog10(ttscl)/1.5d0
      write(*,'(2(a,f5.2),a)')' moment magnitude: ',mw,' ( ',mwsm,
     &    '  rescaled by fitting to the waveforms energy)'
      write(*,'(a,f8.2,a,$)')' peak fault slip: ',maxslp,' m,'
      write(*,'(a,i4,3(a,f6.2),a)')' located at patch no ',isfc,' (',
     &            sflat(isfc),' deg_N, ',sflon(isfc),' deg_E, ',
     &            sfdep(isfc)/KM2M,' km)'
      write(30,'(2(a,f4.2),a)')' moment magnitude: ',mw,' ( ',mwsm,
     &    '  rescaled by fitting to the waveforms energy)'
      write(30,'(a,f6.2,a,$)')' peak fault slip: ',maxslp,' m,'
      write(30,'(a,i4,3(a,f6.2),a)')' located at patch no ',isfc,' (',
     &            sflat(isfc),' deg_N, ',sflon(isfc),' deg_E, ',
     &            sfdep(isfc)/KM2M,' km)'
      close(30)
c
      do isf=1,nsf
        do i=1,nf
          cfct(i)=sstf(i,isf)
        enddo
        m=1
        do i=2*nf,nf+2,-1
          m=m+1
         cfct(i)=dconjg(cfct(m))
        enddo
        cfct(nf+1)=(0.d0,0.d0)
c
        call four1w(cfct,dfct,2*nf,+1)
c
        do i=1,2*nf
          pstf(i,isf)=dmax1(0.d0,dfct(2*i-1)*df)
     &               *sflen(isf)*sfwid(isf)*sfmue(isf)
        enddo
c
        k=1
950     continue
        do i=k,2*nf
          if(pstf(i,isf).gt.0.d0)then
            it1=i
            it2=it1
            do j=i+1,2*nf
              if(pstf(j,isf).le.0.d0)then
                k=j
                if(it2-it1.lt.2)then
                  do itc=it1,it2
                    pstf(itc,isf)=0.d0
                  enddo
                endif
                goto 950
              else
                it2=j
              endif
            enddo
          endif
        enddo
      enddo
c
      deallocate(pkspec,fswap,gswap,clust)
c
      return
      end
