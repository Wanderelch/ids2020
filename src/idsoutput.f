      subroutine idsoutput(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,j,k,i1,i2,isf,ism,igns,isar,m,ind,jnd,knd,it1,it2
      integer*4 jsf,isfp,ipsm
      real*8 slpmax,fac,maxslp,cutslp,delta,sigma,gamma,summa
      real*8 dis,re,rn,vrup,vrupm,t,slw,tkf,maxdep,mindep
      real*8 moment,fsize,fradius,stressdrop,ruptime,risem
      real*8 moment90,ctdlat,ctdlon,ctddep,plunge,azimuth
      character*5 cmptxt
c
      integer*4 system
c
c     output final slip distribution
c
      ierr=system('cd '//outdir)
      if(ierr.ne.0)then
        ierr=system('mkdir '//outdir)
      else
        ierr=system('cd ..')
      endif
      do i=1,120
        if(outdir(i:i).eq.' ')goto 100
      enddo
100   ind=i-1
      if(outdir(ind:ind).eq.'/'.or.outdir(ind:ind).eq.'\')then
        ind=ind-1
        outdir=outdir(1:ind)
      endif
c
      open(31,file=slpmodel,status='unknown')
      write(31,'(a)')'      Lat[deg]      Lon[deg]     Depth[km]'
     &             //'  X_strike[km] Y_downdip[km]'
     &             //'    Length[km]     Width[km]   Strike[deg]'
     &             //'      Dip[deg]     Rake[deg]       Slip[m]'
     &             //'     Moment[Nm]'
c
      do i=1,2*nf
        estf(i)=0.d0
      enddo
c
      it1=1
      it2=itmax
      do isf=1,nsf
        do i=1,it1-1
          pstf(i,isf)=0.d0
        enddo
        do i=it2+1,2*nf
          pstf(i,isf)=0.d0
        enddo
        do i=1,2*nf
          estf(i)=0.d0
        enddo
      enddo
c
      slpmax=0.d0
c
      moment=0.d0
      k=1
      do isf=1,nsf
        sfslp(isf)=0.d0
        fac=sfmue(isf)*sflen(isf)*sfwid(isf)
        do i=it1,it2
          sfslp(isf)=sfslp(isf)+pstf(i,isf)
          estf(i)=estf(i)+pstf(i,isf)
        enddo
        sfslp(isf)=sfslp(isf)*dt/fac
        if(slpmax.lt.sfslp(isf))then
          slpmax=sfslp(isf)
          k=isf
        endif
        moment=moment+fac*sfslp(isf)
c
        write(31,'(11f14.4,E15.4)')sflat(isf),sflon(isf),
     &           sfdep(isf)/KM2M,sfxln(isf)/KM2M,sfywd(isf)/KM2M,
     &           sflen(isf)/KM2M,sfwid(isf)/KM2M,
     &           sfstk(isf),sfdip(isf),sfrak(isf),sfslp(isf),
     &           fac*sfslp(isf)
c
      enddo
      close(31)
      mw=(dlog10(moment)-9.1d0)/1.5d0
c
      open(32,file=estffile,status='unknown')
      write(32,'(a)')'  Time Moment_Rate Accu_Moment      Mw'
      moment=0.d0
      mw=0.d0
      do i=1,it2
        if(moment.gt.0.d0)then
          mw=(dlog10(moment)-9.1d0)/1.5d0
          write(32,'(f7.3,2E12.4,f8.2)')dble(i-1)*dt,estf(i),moment,mw
        else
          write(32,'(f7.3,2E12.4)')dble(i-1)*dt,estf(i),moment
        endif
        moment=moment+estf(i)*dt
      enddo
      close(32)
c
c     output sub-fault source-time-functions
c
      open(33,file=pstffile,status='unknown')
      write(33,'(a7,$)')'   Time'
      cmptxt(1:1)='P'
      do isf=1,nsf
        i=isf/1000
        cmptxt(2:2)=char(ichar('0')+i)
        i=mod(isf,1000)/100
        cmptxt(3:3)=char(ichar('0')+i)
        i=mod(isf,100)/10
        cmptxt(4:4)=char(ichar('0')+i)
        i=mod(isf,10)
        cmptxt(5:5)=char(ichar('0')+i)
        if(isf.lt.nsf)then
          write(33,'(a12,$)')cmptxt
        else
          write(33,'(a12)')cmptxt
        endif
      enddo
      do i=1,it2
        write(33,'(f7.3,$)')dble(i-1)*dt
        do isf=1,nsf-1
          write(33,'(E12.4,$)')pstf(i,isf)
        enddo
        write(33,'(E12.4)')pstf(i,nsf)
      enddo
      close(33)
c
c     output comparisons of observed and synthetic data
c
      do j=1,120
        if(osdir(j:j).eq.' ')goto 401
      enddo
401   jnd=j-1
      if(osdir(jnd:jnd).eq.'/'.or.osdir(jnd:jnd).eq.'\')then
        jnd=jnd-1
        osdir=osdir(1:jnd)
      endif
c
      ierr=system('cd '//osdir)
      if(ierr.ne.0)then
        ierr=system('mkdir '//osdir)
      else
        ierr=system('cd ..')
      endif
c
      do ism=1,nsm
        do k=1,120
          if(smcode(ism)(k:k).eq.' ')goto 402
        enddo
402     knd=k-1
        open(30,file=osdir(1:jnd)//'/'//smcode(ism)(1:knd)
     &             //'_ObsSyn.dat',status='unknown')
        do j=1,3
          do i=1,nf
            cswap(i,j  )=ssmobs(i,j,ism)
            cswap(i,j+3)=ssmsyn(i,j,ism)
          enddo
        enddo
c
        do j=1,6
          m=1
          do i=2*nf,nf+2,-1
            m=m+1
            cswap(i,j)=dconjg(cswap(m,j))
          enddo
          cswap(nf+1,j)=(0.d0,0.d0)
c
          call four1w(cswap(1,j),dswap(1,j),2*nf,+1)
          do i=1,nt
            dswap(i,j)=dswap(2*i-1,j)*df
          enddo
        enddo
        if(nfpsv(ism).eq.0)then
          write(30,'(a)')'     TimeH'
     &                 //'         ObsUe         ObsUn'
     &                 //'         SynUe         SynUn'
     &                 //'     TimeZ'
     &                 //'         ObsUz         SynUz'
          delta=0.d0
        else
          write(30,'(a)')'    TimeSH'
     &                 //'         ObsUe         ObsUn'
     &                 //'         SynUe         SynUn'
     &                 //'     TimeP'
     &                 //'         ObsUz         SynUz'
          delta=tssmsyn(ism)-tpsmsyn(ism)
        endif
        do i=1,nt
          t=dble(i+idnint(t0sm(ism)/dt)-1)*dt
          write(30,'(f10.4,4E14.6,$)')t+delta,dswap(i,1),dswap(i,2),
     &                                        dswap(i,4),dswap(i,5)
          write(30,'(f10.4,2E14.6)')t,dswap(i,3),dswap(i,6)
        enddo
        close(30)
c
        do j=1,3
          mfvar(j,ism)=0.d0
          delta=0.d0
          do i=1,nt
            mfvar(j,ism)=mfvar(j,ism)+(dswap(i,j)-dswap(i,j+3))**2
            delta=delta+dswap(i,j)**2
          enddo
          mfvar(j,ism)=mfvar(j,ism)/delta
        enddo
      enddo
c
c     Normalized residual variance for single wave forms
c
      open(30,file=swfmisfitfile,status='unknown')
      write(30,'(a)')'  Station   Lat[deg]  Lon[deg]'
     &             //' nrv_dsp_e nrv_dsp_n nrv_dsp_z'
      do ism=1,nsm
        write(30,'(a10,2f10.4,3f10.4)')smcode(ism),
     &           smlat(ism),smlon(ism),(mfvar(j,ism),j=1,3)
      enddo
      close(30)
c
c     prediction of static offsets at seismic waveform sites
c
      open(30,file=osmdatafit,status='unknown')
      write(30,'(a)')'   Station'
     &             //'       Lat[deg]       Lon[deg]'
     &             //'          UeObs          UnObs          UpObs'
     &             //'    Sigma_UeObs    Sigma_UnObs    Sigma_UpObs'
     &             //'          UeSyn          UnSyn          UpSyn'
      do ism=1,nsm
        do j=1,3
          sosmsyn(j,ism)=0.d0
          do isf=1,nsf
            sosmsyn(j,ism)=sosmsyn(j,ism)+sfslp(isf)*osmgrn(j,isf,ism)
          enddo
        enddo
        write(30,'(a10,11f15.4)')smcode(ism),smlat(ism),smlon(ism),
     &   (sosmobs(j,ism),j=1,3),(osmstd(j,ism),j=1,3),
     &   (sosmsyn(j,ism),j=1,3)
      enddo
      close(30)
c
c     output gns datafit
c
      if(ngns.gt.0)then
        open(30,file=gnsdatafit,status='unknown')
        write(30,'(a)')'       Lat       Lon'
     &              //'    Ue_Obs    Un_obs    Up_Obs'
     &              //'    Ue_Syn    Un_Syn    Up_Syn'
        do igns=1,ngns
          write(30,'(8f10.3)')gnslat(igns),gnslon(igns),
     &       (sgnsobs(j,igns),j=1,3),(sgnssyn(j,igns),j=1,3)
        enddo
        close(30)
      endif
c
c     output insar datafit
c
      if(nsar.gt.0)then
        open(30,file=sardatafit,status='unknown')
        write(30,'(a)')'       Lat       Lon'
     &              //'    Los_Obs   Los_Syn'
        do isar=1,nsar
          write(30,'(4f10.3)')sarlat(isar),sarlon(isar),
     &              ssarobs(isar),ssarsyn(isar)
        enddo
        close(30)
      endif
c
c     output snapshots
c
      do isf=1,nsf
        do i=1,it1
          pstf(i,isf)=0.d0
        enddo
        fac=sfmue(isf)*sflen(isf)*sfwid(isf)
c
c       convert moment-rate STFs to commulative slip STFs
c
        do i=2,2*nf
          pstf(i,isf)=pstf(i-1,isf)+pstf(i,isf)*dt/fac
        enddo
        pstf(1,isf)=0.d0
      enddo
c
      do j=1,nsnap
        open(30,file=snapshot(j),status='unknown')
        write(30,'(a)')'  Latitude[deg] Longitude[deg]      Depth[km]'
     &               //'   X_strike[km]  Y_downdip[km]'
     &               //'        Slip[m]     Moment[Nm]'
        i1=min0(it2,max0(1,1+idint(t1snap(j)/dt)))
        i2=min0(it2,max0(1,1+idint(t2snap(j)/dt)))
        do isf=1,nsf
          fac=sfmue(isf)*sflen(isf)*sfwid(isf)
          delta=pstf(i2,isf)-pstf(i1,isf)
          write(30,'(6f15.4,E15.6)')sflat(isf),sflon(isf),
     &            sfdep(isf)/KM2M,sfxln(isf)/KM2M,sfywd(isf)/KM2M,
     &            delta,delta*fac
        enddo
        close(30)
      enddo
c
c     earthquake's major parameters
c
      open(30,file=eqmpfile,status='unknown')
c
c     seismic moment, moment magnitude
c
      write(30,'(a,E12.4,a)')' Seismic moment: ',
     &                       moment,' Nm'
      write(30,'(2(a,f5.2),a)')' Moment magnitude: ',mw,' ( ',mwsm,
     &    '  rescaled by fitting to the waveforms energy)'
c
c     fault size
c
      if(idisc.eq.0)then
        write(30,'(a,i10,a)')' Pre-selected potential area: ',
     &    idnint((nsf)*patchsize**2*1.0d-06),' km^2'
      else
        write(30,'(7(a,i4))')' Estimated potential fault size:'
     &    //' length = ',idnint((len1+len2)/KM2M),'(',idnint(len1/KM2M),
     &    ' + ',idnint(len2/KM2M),') km, width = ',
     &    idnint((wid1+wid2)/KM2M),'(',idnint(wid1/KM2M),
     &    ' + ',idnint(wid2/KM2M),') km'
      endif
      write(30,'(a,i10,a)')' Sub-fault size: ',
     &    idnint(patchsize/KM2M),' km'
      write(30,'(a,i10)')' Number of sub-faults: ',nsf
      write(30,'(a,i10,a)')' Displacement signal window used: ',
     &    idnint(tsource),' s'
      write(30,'(a,2(f10.4,a))')' Bandpass filter applied'
     &    //' to displacement waveforms: ',fcut1,' - ',fcut2,' Hz'
      fsize=0.d0
      moment90=0.d0
      do isf=1,nsf
        subf(isf)=.false.
      enddo
500   continue
      cutslp=0.d0
      isfp=0
      do isf=1,nsf
        if(.not.subf(isf).and.cutslp.le.sfslp(isf))then
          cutslp=sfslp(isf)
          isfp=isf
        endif
      enddo
      fsize=fsize+sflen(isfp)*sfwid(isfp)
      moment90=moment90+sfmue(isfp)*sflen(isfp)*sfwid(isfp)*sfslp(isfp)
      subf(isfp)=.true.
      if(moment90.lt.0.90d0*moment)goto 500
      write(30,'(a,i10,a)')' Rupture area (covering 90% moment): ',
     &    idnint(fsize*1.0d-06),' km^2'
c
c     moment centroid
c
      ctdlat=0.d0
      ctdlon=0.d0
      ctddep=0.d0
      do isf=1,nsf
        if(subf(isf))then
          fac=sfmue(isf)*sflen(isf)*sfwid(isf)*sfslp(isf)
          ctdlat=ctdlat+fac*sflat(isf)
          ctdlon=ctdlon+fac*sflon(isf)
          ctddep=ctddep+fac*sfdep(isf)
        endif
      enddo
      ctdlat=ctdlat/moment90
      ctdlon=ctdlon/moment90
      ctddep=ctddep/moment90
      write(30,'(3(a,f6.2),a)')' Centroid of moment distribution: (',
     &     ctdlat,' deg_N, ',ctdlon,' deg_E, ',
     &     ctddep/KM2M,' km)'
c
c     compactness of ruptured area (= 1: homogeneous circular rupture)
c
      maxdep=sfdep(isfhyp)
      mindep=sfdep(isfhyp)
      do isf=1,nsf
        if(subf(isf))then
          maxdep=dmax1(maxdep,sfdep(isf))
          mindep=dmin1(mindep,sfdep(isf))
        endif
      enddo
      if(mindep.lt.1.5d0*patchsize*dsin(sfdip(isfhyp)*DEG2RAD))then
        mindep=0.d0
      endif
      write(30,'(a,2(f8.1,a))')' Depth range of rupture: ',mindep/KM2M,
     &                 ' - ',maxdep/KM2M,' km'
c
c     peak slip and peak-slip location
c
      maxslp=0.d0
      isfp=isfhyp
      do isf=1,nsf
        if(maxslp.lt.sfslp(isf))then
          maxslp=sfslp(isf)
          isfp=isf
        endif
      enddo
      write(30,'(4(a,f6.2),a)')' Peak slip: ',maxslp,
     &     ' m, at (',sflat(isfp),' deg_N, ',sflon(isfp),' deg_E, ',
     &     sfdep(isfp)/KM2M,' km)'
c
c     source duration
c
      ruptime=0.d0
      summa=0.d0
      do i=it1,it2
        summa=summa+estf(i)*dt
        if(summa.le.0.90d0*moment)ruptime=dble(i-it1)*dt
      enddo
      write(30,'(a,f8.1,a)')' Rupture duration'
     &            //' (90% moment release): ',ruptime,' s'
c
      do isf=1,nsf
        sftr(isf)=0.d0
        do i=2,it2
          sftr(isf)=sftr(isf)+(pstf(i,isf)-pstf(i-1,isf))*dble(i-1)*dt
          if(pstf(i,isf).gt.cutslp.and.
     &       pstf(i,isf).gt.0.15d0*pstf(it2,isf))goto 600
        enddo
600     continue
        if(pstf(i,isf).gt.0.d0)then
          sftr(isf)=sftr(isf)/pstf(i,isf)
        else
          sftr(isf)=0.d0
        endif
      enddo
c
      delta=0.d0
      gamma=0.d0
      do isf=1,nsf
        fac=sfmue(isf)*sfslp(isf)*sflen(isf)*sfwid(isf)
     &     *dis3dsf(isf,isfhyp)
        delta=delta+fac*dis3dsf(isf,isfhyp)*sftr(isf)
        gamma=gamma+fac*dis3dsf(isf,isfhyp)**2
      enddo
      vrupm=gamma/delta
      write(30,'(a,f6.2,a)')' Average rupture velocity (based on the'
     &   //' first 15% of local moment release): ',vrupm/KM2M,' km/s'
c
c     rupture direction
c
      call disazi(REARTH,hyplat,hyplon,ctdlat,ctdlon,rn,re)
      azimuth=dmod(360.d0+datan2(re,rn)/DEG2RAD,360.d0)
      plunge=datan2(ctddep-hypdep,dsqrt(re**2+rn**2))/DEG2RAD
c
      write(30,'(2(a,f6.2),a)')' Average rupture direction (hypocenter'
     &    //' to centroid): Plunge = ',plunge,
     &      ' deg, Azimuth = ',azimuth,' deg'
c
      close(30)
c
      open(30,file=rupfrontfile,status='unknown')
      write(30,'(a)')'      Lat[deg]      Lon[deg]     Depth[km]'
     &             //'  X_strike[km] Y_downdip[km]    Ruptime[s]' 
      do isf=1,nsf
        if(sfslp(isf).ge.cutslp)then
          write(30,'(5f14.4,f12.2)')sflat(isf),sflon(isf),
     &                    sfdep(isf)/KM2M,sfxln(isf)/KM2M,
     &                         sfywd(isf)/KM2M,sftr(isf)
        endif
      enddo
      close(30)
      return
      end
