      program ids
      use idsalloc
      implicit none
c
c     work space
c
      integer*4 ierr
c
      integer*4 runtime,time
c
c     read input file file
c
      print *,'######################################################'
      print *,'#                                                    #'
      print *,'#         Welcome to use the program code            #'
      print *,'#                                                    #'
      print *,'#                                                    #'
      print *,'#           III       DDDD         SSSS              #'
      print *,'#            I        D   D       S                  #'
      print *,'#            I        D   D        SSS               #'
      print *,'#            I        D   D           S              #'
      print *,'#           III       DDDD        SSSS               #'
      print *,'#                                                    #'
      print *,'#                  (Version 2020)                    #'
      print *,'#                                                    #'
      print *,'#                      for                           #'
      print *,'#         earthquake finite soure imaging            #'
      print *,'#                   based on                         #'
      print *,'#  the Iterative De-convolution and Stacking method  #'
      print *,'#      -         -                  -                #'
      print *,'#                      by                            #'
      print *,'#                 Rongjiang Wang                     #'
      print *,'#              (wang@gfz-potsdam.de)                 #'
      print *,'#                                                    #'
      print *,'#     GFZ German Research Centre for Geosciences     #'
      print *,'#              last modified: May 2021               #'
      print *,'######################################################'
      print *,'                          '
      write(*,'(a,$)')' the input data file is '
      read(*,'(a)')inputfile
      runtime=time()
c
c     read input parameters
c
      print *,'call idsgetinp'
      call idsgetinp(ierr)
c
c     get crust model
c
      print *,'call idsmodel'
      call idsmodel(ierr)
c
c     get differential Green's function database for static displacement
c
      print *,'call idsdifgrn'
      call idsdifgrn(ierr)
c
c     get and process waveform data
c
      print *,'call idssmdat'
      call idssmdat(ierr)
c
c     get Green's functions for static displacement at waveform data sites
c
      print *,'call idsosmgrn'
      call idsosmgrn(ierr)
c
c     get Green's functions for seismic waveforms
c
      print *,'call idssmgrn'
      call idssmgrn(ierr)
c
c     re-weight waveform data considering station distribution
c
      print *,'call idssmwei'
      call idssmwei(ierr)
c
      if(ngns.gt.0)then
c
c       get gns data
c
        print *,'call idsgnsdat'
        call idsgnsdat(ierr)
c
c       get Green's functions for gns data
c
        print *,'call idsgnsgrn'
        call idsgnsgrn(ierr)
      endif
      if(nsar.gt.0)then
c
c       get InSAR data
c
        print *,'call idssardat'
        call idssardat(ierr)
c
c       get Green's functions for InSAR data
c
        print *,'call idssargrn'
        call idssargrn(ierr)
      endif
c
c     perform joint source inversion
c
      print *,'call idsinverse'
      call idsinverse(ierr)
c
c     output results
c
      print *,'call idsoutput'
      call idsoutput(ierr)
c
      runtime=time()-runtime
      write(*,'(a)')' #############################################'
      write(*,'(a)')' #                                           #'
      write(*,'(a)')' #     End of computations with ids2020      #'
      write(*,'(a)')' #                                           #'
      write(*,'(a,i10,a)')' #       Run time: ',runtime,
     &                                           ' sec            #'
      write(*,'(a)')' #############################################'
      stop
      end
