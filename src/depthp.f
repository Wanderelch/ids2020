      subroutine depthp(nz,h,vp,vs,zs,slwph,ttpp,ttsp,ierr)
      implicit none
c
c     Input:
c
      integer*4 nz,ierr
      real*8 zs,slwph
      real*8 h(nz),vp(nz),vs(nz)
c
c     Return:
c
      real*8 ttpp,ttsp
c
      integer*4 i,i0
      real*8 z,z0,slw,slwz,dpp,dsp,dps,dss
c
      ierr=0
      z=0.d0
      do i=1,nz
        z=z+h(i)
        if(z.le.zs)then
          i0=i
          z0=z
        endif
      enddo
c
      ttpp=0.d0
      dpp=0.d0
      ttsp=0.d0
      dsp=0.d0
      do i=1,i0
        if(i.lt.i0)then
          z=h(i)
        else
          z=zs-z0
        endif
        slw=1.d0/vp(i)
        if(slwph.gt.slw)then
          ierr=1
          return
        endif
        slwz=dsqrt(slw**2-slwph**2)
        ttpp=ttpp+z*slw**2/slwz
        dpp=dpp+z*slwph/slwz
c
        slw=1.d0/vs(i)
        if(slwph.gt.slw)then
          ierr=2
          return
        endif
        slwz=dsqrt(slw**2-slwph**2)
        ttsp=ttsp+z*slw**2/slwz
        dsp=dsp+z*slwph/slwz
      enddo
      ttsp=ttpp+ttsp-(dpp+dsp)*slwph
      ttpp=2*ttpp-2.d0*dpp*slwph
c
      return
      end