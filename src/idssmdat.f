      subroutine idssmdat(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 ndegmax
      parameter(ndegmax=10)
c
      integer*4 i,j,k,k1,k2,m,i1,i2,isf,jsf,ism,ind,jnd,knd
      integer*4 ipre,ipsm,ipebc,itap,iwin,ismtype0,keyp,keys,ndeg,nwebc
      integer*4 year0,month0,day0,hour0,minute0,nw1,nw2,nwsm,ntd,nfd
      integer*4 icmp(3),i3ps(3),i4ps(3)
      real*8 a,b,c,d,e,tau,f,t,lat0,lon0,dep0,bazi,dfd
      real*8 alpha,beta,tap,deltp,stretch,dis,dtd,dtebc
      real*8 tpp,tsp,tps,tss,ttp,tkfp,slwp,tts,tkfs,slws
      real*8 smlat0,smlon0,re,rn,delta,gamma,sigma,eta1,eta2
      real*8 tlv,smin,tref,vt,vp,dtap,csbhyp,ssbhyp
      real*8 puff(10)
      character*180 text
      logical*2 ebc
c
c     read station info
c
      do k=1,120
        if(ebcdir(k:k).eq.' ')goto 10
      enddo
10    knd=k-1
      do i=1,120
        if(smobsdir(i:i).eq.' ')goto 20
      enddo
20    ind=i-1
      if(smobsdir(ind:ind).ne.'/'.and.smobsdir(ind:ind).ne.'\')then
        smobsdir=smobsdir(1:ind)//'\'
        ind=ind+1
      endif
      open(20,file=smobsdir(1:ind)//'StationInfo.dat',status='old')
      call skipdoc(20)
      read(20,*)year0,month0,day0,hour0,minute0,second
      if(year0.ne.year.or.month0.ne.month.or.day0.ne.day.or.
     &   hour.ne.hour0.or.minute0.ne.minute)then
        stop ' Error in idssmdat: inconsistent event origin time!'
      endif
      tref=second-hyptime
      call skipdoc(20)
      read(20,*)lat0,lon0,dep0
      dep0=dep0*KM2M
      if(lat0.ne.hyplat.or.lon0.ne.hyplon.or.dep0.ne.hypdep)then
        stop ' Error in idssmdat: inconsistent hypocentre location!'
      endif
      call skipdoc(20)
      read(20,*)nsm
c
      allocate(smcode(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: smcode not allocated!'
      allocate(ismtype(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: ismtype not allocated!'
      allocate(sampling(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: sampling not allocated!'
      allocate(smlat(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: smlat not allocated!'
      allocate(smlon(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: smlon not allocated!'
      allocate(epidissm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: epidissm not allocated!'
      allocate(t0sm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: t0sm0 not allocated!'
      allocate(twsm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: twsm not allocated!'
      allocate(tpsm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: tpsm not allocated!'
      allocate(tssm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: tssm not allocated!'
      allocate(tpsmsyn(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: tpsmsyn not allocated!'
      allocate(tssmsyn(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: tssmsyn not allocated!'
      allocate(smobsazi(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: smobsazi not allocated!'
      allocate(psmtkf(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: psmtkf not allocated!'
      allocate(psmslw(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: psmslw not allocated!'
      allocate(hypdis(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: hypdis not allocated!'
      allocate(smwei(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: smwei not allocated!'
      allocate(smwei0(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: smwei0 not allocated!'
      allocate(osmstd(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: osmstd not allocated!'
      allocate(osmwei(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: osmwei not allocated!'
      allocate(nfpsv(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: nfpsv not allocated!'
      allocate(nfsh(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: nfsh not allocated!'
      allocate(tppsm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: tppsm not allocated!'
      allocate(tspsm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: tspsm not allocated!'
      allocate(tpssm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: tpssm not allocated!'
      allocate(tsssm(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: tsssm not allocated!'
      allocate(twincut(nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: twincut not allocated!'
c
      allocate (mfvar(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: mfvar not allocated!'
c
      call skipdoc(20)
      read(20,*)(icmp(i),i=1,3)
c
      j=0
      do ism=1,nsm
        call skipdoc(20)
        read(20,*)text,smlat0,smlon0,(puff(i),i=1,6),ismtype0
        call disazi(REARTH,hyplat,hyplon,
     &                     smlat0,smlon0,rn,re)
        dis=dsqrt(re*re+rn*rn)
        if(dis.gt.stdismin.and.dis.lt.stdismax)then
          call idspstt(dis,hypdep,ttp,tkfp,slwp,tts,tkfs,slws)
          if(swindow.gt.tts-ttp+trise+ttap)then
c
c           near-field station
c
            keyp=0
            tpp=0.d0
            tsp=0.d0
c
            keys=0
            tps=0.d0
            tss=0.d0
c
            sigma=tts-ttp+trise+tpst
          else
            call depthphase(REARTH,nlayer,hpmod,vpmod,vsmod,hypdep,
     &                      tkfp,tpp,tsp,tkfs,tps,tss,ierr)
            if(tsource.lt.tts-ttp-2.d0*ttap)then
c
c             far-field station
c
              delta=dmin1(swindow,tts-ttp-2.d0*ttap)
c
              sigma=0.d0
              if(tsp+trise+2.d0*ttap.lt.delta)then
                keyp=1
                sigma=dmax1(sigma,tsp+trise+tpst)
              else if(tpp+trise+2.d0*ttap.lt.dmin1(tsp,delta))then
                keyp=2
                sigma=dmax1(sigma,tpp+trise+tpst)
              else if(trise+2.d0*ttap.lt.tpp)then
                sigma=dmax1(sigma,trise+tpst)
                keyp=3
              else
                keyp=-1
              endif
c
              if(tss+trise+2.d0*ttap.lt.delta)then
                keys=1
                sigma=dmax1(sigma,tss+trise+tpst)
              else if(tps+trise+2.d0*ttap.lt.dmin1(tss,delta))then
                keys=2
                if(tps.le.0.d0)keys=3
                sigma=dmax1(sigma,tps+trise+tpst)
              else if(trise+2.d0*ttap.lt.tps)then
                keys=3
                sigma=dmax1(sigma,trise+tpst)
              else
                keys=-1
              endif
            else
              keyp=-1
              keys=-1
            endif
          endif
c
          if(keyp.ge.0)then
            j=j+1
            nfpsv(j)=keyp
            nfsh(j)=keys
            tppsm(j)=tpp
            tspsm(j)=tsp
            tpssm(j)=tps
            tsssm(j)=tss
c
            twincut(j)=sigma
c
            backspace(20)
            read(20,*)smcode(j),smlat(j),smlon(j),
     &                t0sm(j),tpsm(j),twsm(j),(smwei0(i,j),i=1,3),
     &                ismtype(j),sampling(j)
            if(ismtype(j).lt.0.or.ismtype(j).gt.2)then
              stop 'Error in idssmdat: wrong data type!'
            endif
c
            epidissm(j)=dis
            smobsazi(j)=dmod(360.d0+datan2(re,rn)/DEG2RAD,360.d0)
            tpsmsyn(j)=ttp
            tssmsyn(j)=tts
            psmtkf(j)=tkfp
            psmslw(j)=slwp
            t0sm(j)=t0sm(j)+tref
            if(tpsm(j).gt.0.d0)then
              tpsm(j)=tpsm(j)+tref
              tssm(j)=tpsm(j)+tts-ttp
            else
              tpsm(j)=tpsmsyn(j)
              tssm(j)=tssmsyn(j)
            endif
c
            if(nfpsv(j).eq.0)then
              delta=twincut(j)+tpsm(j)-t0sm(j)
            else
              delta=twincut(j)+tssm(j)-t0sm(j)
            endif
c
            twsm(j)=delta+tpre
c
            if(tpsm(j).le.t0sm(j))j=j-1
          endif
        endif
      enddo
      nsm=j
      close(20)
      write(*,'(i6,a)')nsm,' waveform sites have been selected.'
      if(nsm.lt.3)then
        stop ' Error in idssmdat: too less waveform sites!'
      endif
c
c     number of nearfield stations
c
      nearfield=0
      do ism=1,nsm
        if(nfpsv(ism).eq.0)nearfield=nearfield+1
      enddo
c
      allocate(ssmobs(nf,3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: ssmobs not allocated!'
      allocate(dsmobs(nf,3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: dsmobs not allocated!'
      allocate(ssmsyn(nf,3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: ssmsyn not allocated!'
      allocate(dsmsyn(nf,3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: dsmsyn not allocated!'
c
      allocate(sosmobs(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: sosmobs not allocated!'
      allocate(dosmobs(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: dosmobs not allocated!'
      allocate(sosmsyn(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: sosmsyn not allocated!'
      allocate(dosmsyn(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: dosmsyn not allocated!'
      allocate(smobsdur(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmdat: smobsdur not allocated!'
c
c     end of reading station info
c
      write(*,'(a)')'  station      dis[km]    azi[deg]   tp_obs[s]'
     &         //'   tp_dev[s]  p_tkf[deg] p_slw[s/km]  sig_dur[s]'
     &         //'       near/far'
      do ism=1,nsm
        dtd=sampling(ism)
        nwsm=1+idint(twsm(ism)/dtd)
        iwin=1+idint(twindow/dtd)
c
        ntd=1
30      ntd=2*ntd
        if(ntd.lt.iwin)goto 30
        dfd=1.d0/(dble(ntd)*dtd)
        nfd=ntd/2
c
        allocate(fswap(nwsm),stat=ierr)
        if(ierr.ne.0)stop ' Error in idssmdat: fswap not allocated!'
        allocate(csmd(2*nfd),stat=ierr)
        if(ierr.ne.0)stop ' Error in idssmdat: csmd not allocated!'
        allocate(dsmd(max0(nwsm,4*nfd),3),stat=ierr)
        if(ierr.ne.0)stop ' Error in idssmdat: dsmd not allocated!'
        allocate(discor(nwsm,3),stat=ierr)
        if(ierr.ne.0)stop ' Error in idssmdat: discor not allocated!'
        allocate(velcor(nwsm,3),stat=ierr)
        if(ierr.ne.0)stop ' Error in idssmdat: velcor not allocated!'
        allocate(velbserr(nwsm,3),stat=ierr)
        if(ierr.ne.0)stop ' Error in idssmdat: velbserr not allocated!'
c
        do j=1,120
          if(smcode(ism)(j:j).eq.' ')goto 100
        enddo
100     jnd=j-1
        open(21,file=smobsdir(1:ind)//smcode(ism)(1:jnd)//'.dat',
     &       status='old')
c
        delta=tpsm(ism)-t0sm(ism)
        if(delta.gt.tpre)then
          k=idint((delta-tpre)/dtd)
          do i=1,k
            read(21,*)(fswap(j),j=1,3)
          enddo
          nw1=1
        else
          nw1=1+idint((tpre-delta)/dtd)
        endif
c
        t0sm(ism)=tpsm(ism)-tpre
c
        do i=1,nw1-1
          do j=1,3
            dsmd(i,j)=0.d0
          enddo
        enddo
        nw2=nwsm
        do i=nw1,nwsm
          read(21,*,end=110)(fswap(j),j=1,3)
          do j=1,3
            dsmd(i,icmp(j))=fswap(j)
          enddo
          nw2=i
        enddo
110     close(21)
c
        do i=nw2+1,ntd
          do j=1,3
            dsmd(i,j)=0.d0
          enddo
        enddo
c
        ipsm=1+idint((tpsm(ism)-t0sm(ism))/dtd)
c
c       remove pre-seismic offset
c
        k=1+(ipsm+nw1)/2
c
        do j=1,3
          delta=0.d0
          do i=nw1,k
            delta=delta+dsmd(i,j)
          enddo
          delta=delta/dble(1+k-nw1)
          do i=nw1,nw2
            dsmd(i,j)=dsmd(i,j)-delta
          enddo
        enddo
c
        if(nfpsv(ism).gt.0)then
          iwin=1+idint(twincut(ism)/dtd)
          m=1+idint((tssm(ism)-tpsm(ism))/dtd)
          if(nwsm.lt.ipsm+iwin+m)then
            do j=1,2
              smwei0(j,ism)=0.d0
              do i=1,nwsm
                dsmd(i,j)=0.d0
              enddo
            enddo
          else
            i1=min0(ipsm+iwin,nwsm-m-1)
            do j=1,2
              do i=1,i1
                dsmd(i,j)=dsmd(i+m-1,j)
              enddo
              do i=1+i1,min0(ipsm+iwin,nwsm)
                dsmd(i,j)=0.d0
              enddo
            enddo
            nwsm=i1
c
c           get sh waves
c
            call disazi(REARTH,smlat(ism),smlon(ism),
     &                         hyplat,hyplon,rn,re)
c
c           bazi = Pi + azimuth (from south to east) of subfault relative to station
c
            bazi=PI+datan2(re,-rn)
            ssbhyp=dsin(bazi)
            csbhyp=dcos(bazi)
c
            do i=1,nwsm
              delta=dsmd(i,1)*csbhyp+dsmd(i,2)*ssbhyp
              dsmd(i,1)=delta*csbhyp
              dsmd(i,2)=delta*ssbhyp
            enddo
          endif
        endif
c
c       convert displacement or velocity to acceleration
c
        do k=1,2-ismtype(ism)
          do j=1,3
            do i=nw1,nw2-1
              dsmd(i,j)=(dsmd(i+1,j)-dsmd(i,j))/dtd
            enddo
            dsmd(nw2,j)=dsmd(nw2-1,j)
          enddo
        enddo
c
c       get static offset using empirical baseline correction
c
        ebc=osmsetwei.gt.0.d0.and.nfpsv(ism).eq.0.and.
     &      nw1.eq.1.and.nw2.eq.nwsm
c
        if(ebc)then
          m=min0(10,1+idint(dtgrn/dtd))
          dtebc=dble(m)*dtd
          nwebc=nwsm/m
          ipebc=ipsm/m
          do j=1,3
            velcor(1,j)=0.d0
            do i=2,nwebc
              delta=0.d0
              do k=1+m*(i-2),m*(i-1)
                delta=delta+dsmd(k,j)*dtd
              enddo
              velcor(i,j)=velcor(i-1,j)+delta
            enddo
            call bscmono(nwebc,ipebc,velcor(1,j),velbserr(1,j),
     &                   dtebc,sosmobs(j,ism),sigma)
            osmstd(j,ism)=sigma
          enddo
          open(31,file=ebcdir(1:knd)//'/'//smcode(ism)(1:jnd)
     &              //'_base.dat',status='unknown')
          write(31,'(a)')'        time'
     &                 //'        Ve_org        Vn_org        Vz_org'
     &                 //'        Ve_err        Vn_err        Vz_err'
     &                 //'        Ve_cor        Vn_cor        Vz_cor'
     &                 //'        Ue_cor        Un_cor        Uz_cor'
          do j=1,3
            discor(1,j)=0.d0
            do i=2,nwebc
              discor(i,j)=discor(i-1,j)+velcor(i,j)*dtebc
            enddo
          enddo
          do i=1,nwebc
            write(31,'(f12.6,12E14.6)')t0sm(ism)+dble(i-1)*dtebc,
     &            (velcor(i,j)-velbserr(i,j),j=1,3),
     &            (velbserr(i,j),j=1,3),(velcor(i,j),j=1,3),
     &            (discor(i,j),j=1,3)
          enddo
          close(31)
        else
          do j=1,3
            sosmobs(j,ism)=0.d0
            osmstd(j,ism)=0.d0
          enddo
        endif
c
c       estimate maximum signal window
c
        itap=1+idint(dmin1(tpre,tpst,ttap)/dtd)
        i1=max0(1,ipsm-itap)
        i2=ipsm
        if(nfpsv(ism).eq.0)then
          delta=0.d0
          do isf=1,nsf
            call disazi(REARTH,sflat(isf),sflon(isf),
     &                  smlat(ism),smlon(ism),rn,re)
            dis=dsqrt(re*re+rn*rn)
            call idspstt(dis,sfdep(isf),ttp,tkfp,slwp,tts,tkfs,slws)
            delta=dmax1(delta,tts-ttp)
          enddo
          gamma=tssmsyn(ism)-tpsmsyn(ism)
          iwin=1+idint(dmin1(swindow-ttap,
     &                       0.5d0*(delta+gamma)+tsource)/dtd)
c
          do j=1,3
            i3ps(j)=min0(ipsm+iwin,nwsm)
          enddo
        else
          if(nfpsv(ism).eq.1)then
            iwin=1+idint(dmin1(swindow-ttap,tspsm(ism)+tsource,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          else if(nfpsv(ism).eq.2)then
            iwin=1+idint(dmin1(swindow-ttap,tppsm(ism)+trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          else
            iwin=1+idint(dmin1(swindow-ttap,trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          endif
c
          i3ps(3)=min0(ipsm+iwin,nwsm)
c
          if(nfsh(ism).eq.1)then
            iwin=1+idint(dmin1(swindow-ttap,tsssm(ism)+tsource,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          else if(nfsh(ism).eq.2)then
            iwin=1+idint(dmin1(swindow-ttap,tpssm(ism)+trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          else
            iwin=1+idint(dmin1(swindow-ttap,trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          endif
          do j=1,2
            i3ps(j)=min0(ipsm+iwin,nwsm)
          enddo
        endif
c
        do j=1,3
          do i=1,i2
            fswap(i)=0.d0
          enddo
          do i=i2+1,i3ps(j)
            fswap(i)=fswap(i-1)+(dsmd(i,j)-dsmd(i-1,j))**2
          enddo
          m=i2
          do i=i2+1,i3ps(j)
            if(fswap(i).le.0.85d0*fswap(i3ps(j)))m=i
          enddo
          smobsdur(j,ism)=dble(m-i2)*dtd
        enddo
c
        gamma=0.d0
        sigma=0.d0
        do j=1,3
          gamma=gamma+smwei0(j,ism)*smobsdur(j,ism)
          sigma=sigma+smwei0(j,ism)
        enddo
        if(sigma.le.0.d0)then
          gamma=(smobsdur(1,ism)+smobsdur(2,ism)+smobsdur(3,ism))/3.d0
        else
          gamma=gamma/sigma
        endif
        write(*,'(a10,6f12.4,f12.1,$)')smcode(ism),epidissm(ism)/KM2M,
     &          smobsazi(ism),tpsm(ism),tpsm(ism)-tpsmsyn(ism),
     &          psmtkf(ism),psmslw(ism)*KM2M,gamma
        if(nfpsv(ism).eq.0)then
          write(*,'(a)')'     near_field'
        else
          write(*,'(a12,i1,a1,i1)')'  far_field_',
     &            nfpsv(ism),'_',nfsh(ism)
        endif
c
c       estimate usable signal window
c
        if(nfpsv(ism).eq.0)then
          iwin=1+idint(dmin1(swindow-ttap,
     &                      tssmsyn(ism)-tpsmsyn(ism)+tsource)/dtd)
c
          do j=1,3
            i3ps(j)=ipsm+iwin
            i4ps(j)=i3ps(j)+itap
          enddo
        else
          if(nfpsv(ism).eq.1)then
            iwin=1+idint(dmin1(swindow-ttap,tspsm(ism)+trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          else if(nfpsv(ism).eq.2)then
            iwin=1+idint(dmin1(swindow-ttap,tppsm(ism)+trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          else
            iwin=1+idint(dmin1(swindow-ttap,trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          endif
c
          i3ps(3)=ipsm+iwin
          i4ps(3)=i3ps(3)+itap
c
          if(nfsh(ism).eq.1)then
            iwin=1+idint(dmin1(swindow-ttap,tsssm(ism)+trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          else if(nfsh(ism).eq.2)then
            iwin=1+idint(dmin1(swindow-ttap,tpssm(ism)+trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          else
            iwin=1+idint(dmin1(swindow-ttap,trise+ttap,
     &           tssmsyn(ism)-tpsmsyn(ism)-2.d0*ttap)/dtd)
          endif
c
          do j=1,2
            i3ps(j)=ipsm+iwin
            i4ps(j)=i3ps(j)+itap
          enddo
        endif
c
c       supress pre- and post-event noise
c
        do j=1,3
          do i=1,i1
            dsmd(i,j)=0.d0
          enddo
          do i=i1,i2
            dsmd(i,j)=dsmd(i,j)
     &               *dsin(0.5d0*PI*dble(i-i1)/dble(i2-i1))**2
          enddo
          do i=i3ps(j),nwsm
            dsmd(i,j)=dsmd(i,j)
     &               *dexp(-(dble(i-i3ps(j))*dtd/ttap)**2)
          enddo
        enddo
c
c       bandpass filtering
c
        allocate(bpf0(nfd),stat=ierr)
        if(ierr.ne.0)stop ' Error in idssmdat: bpf0 not allocated!'
        allocate(bpfd(nfd),stat=ierr)
        if(ierr.ne.0)stop ' Error in idssmdat: bpfd not allocated!'
c
        if(fcut1g.le.0.d0)then
          call butterworth(nbpfg,fcut2g,dfd,nfd,bpf0)
        else
          call bandpass(nbpfg,fcut1g,fcut2g,dfd,nfd,bpf0)
        endif
        call bandpass(nbpfids,fcut1,fcut2,dfd,nfd,bpfd)
        do i=1,nfd
          bpfd(i)=bpfd(i)*bpf0(i)
        enddo
c
        do j=1,3
          do i=nwsm+1,2*nfd
            dsmd(i,j)=0.d0
          enddo
c
          do i=1,2*nfd
            csmd(i)=dcmplx(dsmd(i,j),0.d0)
          enddo
c
          call four1w(csmd,dsmd(1,j),2*nfd,-1)
c
          do i=1,nfd
            csmd(i)=csmd(i)*bpfd(i)*dcmplx(dfd*dtd,0.d0)
          enddo
          m=1
          do i=2*nfd,nfd+2,-1
            m=m+1
            csmd(i)=dconjg(csmd(m))
          enddo
          csmd(nfd+1)=(0.d0,0.d0)
c
          call four1w(csmd,dsmd(1,j),2*nfd,+1)
c
c         coverting to velocity
c
          dsmd(1,j)=0.d0
          do i=2,2*nfd
            dsmd(i,j)=dsmd(i-1,j)+dreal(csmd(i))*dtd
          enddo
        enddo
c
        deallocate(bpf0,bpfd)
c
c       coverting to displacement and re-sampling from dtd (of the original data)
c       to the uniform dt shift the times series so that the P arrival is aligned
c       to the theoretical one
c
        t0sm(ism)=t0sm(ism)+tpsmsyn(ism)-tpsm(ism)
        tpsm(ism)=tpsmsyn(ism)
c
        do j=1,3
          dfct(1)=0.d0
          do i=2,2*nf
            t=dble(i-1)*dt
            k1=1+idint((t-0.5d0*dt)/dtd)
            k2=1+idint((t+0.5d0*dt)/dtd)
            if(k1.le.1.or.k2.ge.ntd)then
              dfct(i)=0.d0
            else
              e=0.d0
              do m=1+k1,k2
                e=e+dsmd(m,j)
              enddo
              b=dmod(t-0.5d0*dt,dtd)/dtd
              a=1.d0-b
              d=dmod(t+0.5d0*dt,dtd)/dtd
              c=1.d0-d
              dfct(i)=dfct(i-1)+(0.5d0*(a*dsmd(k1,j)+b*dsmd(k1+1,j)
     &               +c*dsmd(k2,j)+d*dsmd(k2+1,j))+e)*dt/dble(1+k2-k1)
            endif
          enddo
c
          do i=1,2*nf
            cfct(i)=dcmplx(dfct(i)*dt,0.d0)
          enddo
c
          call four1w(cfct,dfct,2*nf,-1)
c
          do i=1,nf
            ssmobs(i,j,ism)=cfct(i)
          enddo
        enddo
c
        deallocate(fswap,csmd,dsmd,discor,velcor,velbserr)
c
      enddo
c
      return
      end