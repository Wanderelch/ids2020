      subroutine idsgnsdat(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,igns
      character*80 header
      real*8 sigma
c
c     allocation of gns data
c
      allocate(gnslat(ngns),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgnsdat: gnslat not allocated!'
      allocate(gnslon(ngns),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgnsdat: gnslon not allocated!'
      allocate(gnswei(3,ngns),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgnsdat: gnswei not allocated!'
      allocate(sgnsobs(3,ngns),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgnsdat: sgnsobs not allocated!'
      allocate(dgnsobs(3,ngns),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgnsdat: dgnsobs not allocated!'
      allocate(sgnssyn(3,ngns),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgnsdat: sgnssyn not allocated!'
      allocate(dgnssyn(3,ngns),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgnsdat: dgnssyn not allocated!'
c
      open(20,file=gnsdatafile,status='old')
      read(20,'(a1)')header
      sigma=0.d0
      do igns=1,ngns
        read(20,*)gnslat(igns),gnslon(igns),
     &            (sgnsobs(i,igns),i=1,3),(gnswei(i,igns),i=1,3)
        do i=1,3
          gnswei(i,igns)=1.d0/gnswei(i,igns)**2
          sigma=sigma+gnswei(i,igns)
        enddo
      enddo
      do igns=1,ngns
        do i=1,3
          gnswei(i,igns)=gnswei(i,igns)/sigma
        enddo
      enddo
      close(20)
      return
      end