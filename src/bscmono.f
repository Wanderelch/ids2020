      subroutine bscmono(nw,ipp,vel,bse,dt,doffset,sigma)
      implicit none
c
c     nw: length of velocity time series
c     ipp: position of P arrival
c     dt: sampling interval
c     vel(nw): input and returned velocity time series
c     bse(nw): errors estimated based on empirical baseline correction
c     doffset: returned estimate of permanent displacement offset
c     sigma: estimated uncertainty of doffset
c
      integer*4 nw,ipp
      real*8 dt,doffset,sigma
      real*8 vel(nw),bse(nw)
c
      integer*4 i,j,i0,i1,i2,i3,i4,i5,i6,iw,ierr
      integer*4 k,k1,k2,k3,k4,k5,k6,ks,id,ip,npeak,ipga
      real*8 a0,a1,delta,smin,gamma,pga,pgv
      real*8 d1,d2,d3,d4,d5,d6,d1opt,d2opt,d3opt,d4opt,d5opt
c
      integer*4 ndeg
      parameter(ndeg=2)
c
      real*8 pi,sdw
      data pi,sdw/3.14159265358979d+00,0.75d+00/
c
      real*8, allocatable:: poly(:,:),bat(:),swp(:)
      allocate (poly(nw,0:ndeg),stat=ierr)
      if(ierr.ne.0)stop ' Error in bscmono: poly not allocated!'
      allocate (bat(0:ndeg),stat=ierr)
      if(ierr.ne.0)stop ' Error in bscmono: bat not allocated!'
      allocate (swp(nw),stat=ierr)
      if(ierr.ne.0)stop ' Error in bscmono: swp not allocated!'
c
c     remove pre-event trend
c
      ip=ipp
      call d2dfit(ip,vel,poly,bat,1,bse)
c
      a0=bse(1)
      a1=(bse(ip)-bse(1))/dble(ip-1)
c
      do i=1,nw
        vel(i)=vel(i)-(a0+a1*dble(i-1))
      enddo
c
c     uncorrected acceleration
c
      do i=2,nw
        swp(i)=(vel(i)-vel(i-1))/dt
      enddo
      swp(1)=swp(2)
c
c     estimate pga time
c
      ipga=ip
      pga=dabs(swp(ip))
      do i=ip+1,nw
        if(pga.lt.dabs(swp(i)))then
          ipga=i
          pga=dabs(swp(i))
        endif
      enddo
c
      swp(1)=0.d0
      do i=2,nw
        swp(i)=swp(i-1)+swp(i)**2
      enddo
c
c     estimate energy signal window
c
      iw=ip
      do i=ip+1,nw
        if(swp(i).ge.sdw*swp(nw))then
          iw=i
          goto 100
        endif
      enddo
100   continue
c
      iw=min0((2*nw+ip)/3,max0(iw,2*ipga-ip))
c
c     estimate earliest start time of baseline shift
c
      swp(1)=0.d0
      do i=2,nw
        swp(i)=swp(i-1)+vel(i)*dt
      enddo
c
      i0=ip
      do i=1+(2*ip+ipga)/3,ip+1,-1
        if(swp(i)*swp(i-1).le.0.d0)then
          i0=i
          goto 200
        endif
      enddo
200   continue
      ip=i0
c
c     main smoothing procedure
c
      i0=1+(2*nw+iw)/3
      call d2dfit(2+nw-i0,swp(i0-1),poly,bat,2,bse(i0-1))
      do i=i0,nw
        swp(i)=(bse(i)-bse(i-1))/dt
      enddo
      d1=(swp(nw)-swp(i0))/dble(nw-i0)
c
      do i=1,ip
        bse(i)=0.d0
      enddo
      do i=ip+1,i0
        bse(i)=vel(i)
      enddo
      do i=i0+1,nw
        bse(i)=vel(i)*dcos(0.5d0*pi*dble(i-i0)/dble(nw-i0))**2
     &        +swp(i)*dsin(0.5d0*pi*dble(i-i0)/dble(nw-i0))**2
      enddo
c
      k=0
400   k=k+1
      do i=ip,nw
        swp(i)=bse(i)
      enddo
      do i=ip+1,nw-1
        bse(i)=(swp(i-1)+swp(i)+swp(i+1))/3.d0
      enddo
c
      npeak=0
      do i=iw+1,nw-1
        if(bse(i).lt.bse(i-1)+d1.and.
     &     bse(i).lt.bse(i+1)-d1.or.
     &     bse(i).gt.bse(i-1)+d1.and.
     &     bse(i).gt.bse(i+1)-d1)npeak=npeak+1
      enddo
      if(npeak.gt.1)goto 400
c
      gamma=0.d0
      do i=iw,nw
        gamma=dmax1(gamma,dabs(vel(i)-bse(i)))
      enddo
c
      pgv=gamma
      do i=1,iw-1
        pgv=dmax1(pgv,dabs(vel(i)-bse(i)))
      enddo
c
      i0=iw
      npeak=0
      do i=iw-1,ip+1,-1
        gamma=dmax1(gamma,dabs(vel(i)-bse(i)))
        if(bse(i).lt.bse(i-1)+d1.and.
     &     bse(i).lt.bse(i+1)-d1.or.
     &     bse(i).gt.bse(i-1)+d1.and.
     &     bse(i).gt.bse(i+1)-d1)npeak=npeak+1
        if(npeak.gt.0.or.
     &     gamma.ge.0.25d0*pgv.and.dabs(bse(i)).le.0.5d0*pgv)then
          goto 500
        else
          i0=i
        endif
      enddo
500   continue
      if(npeak.gt.0)then
        iw=(2*i0+iw)/3
      else
        iw=i0
      endif
c
      call d2dfit(1+nw-iw,bse(iw),poly,bat,1,swp(iw))
c
      d1=swp(iw+1)-swp(iw)
      d2=swp(iw)/dble(iw-ip)
      if(d2*d1.ge.0.d0.and.dabs(d2).lt.dabs(d1))then
        ip=(ip+iw-idint(swp(iw)/d1))/2
      endif
c
      do i=1,iw
        bse(i)=0.d0
      enddo
c
c     estimate best-fit monotonic trend within signal window by grid search
c     assuming that baseline shift increases faster than ~sqrt(t)
c
      id=1+(iw-ip)/6
      if(ip+6*id.ge.nw)id=id-1
      i1=ip+id
      i2=i1+id
      i3=i2+id
      i4=i3+id
      i5=i4+id
      i6=i5+id
      smin=0.d0
c
      k6=18
      d6=bse(i6)
      delta=d6/18.d0
c
      do k5=0,16
        d5=dble(k5)*delta
        do k4=0,min0(15,k5)
          d4=dble(k4)*delta
          do k3=0,min0(13,k4)
            d3=dble(k3)*delta
            do k2=0,min0(10,k3)
              d2=dble(k2)*delta
              do k1=0,min0(7,k2)
                d1=dble(k1)*delta
                sigma=0.d0
                do i=ip+1,i1
                  sigma=sigma+(vel(i)
     &                 -d1*(dble(i-ip)/dble(id))**2)**2
                enddo
                do i=i1+1,i2
                  sigma=sigma+(vel(i)
     &                 -(d1+(d2-d1)*dble(i-i1)/dble(id)))**2
                enddo
                do i=i2+1,i3
                  sigma=sigma+(vel(i)
     &                 -(d2+(d3-d2)*dble(i-i2)/dble(id)))**2
                enddo
                do i=i3+1,i4
                  sigma=sigma+(vel(i)
     &                 -(d3+(d4-d3)*dble(i-i3)/dble(id)))**2
                enddo
                do i=i4+1,i5
                  sigma=sigma+(vel(i)
     &                 -(d4+(d5-d4)*dble(i-i4)/dble(id)))**2
                enddo
                do i=i5+1,i6
                  sigma=sigma+(vel(i)
     &                 -(d5+(d6-d5)*dble(i-i5)/dble(id)))**2
                enddo
                if(smin.le.0.d0.or.sigma.le.smin)then
                  smin=sigma
                  d1opt=d1
                  d2opt=d2
                  d3opt=d3
                  d4opt=d4
                  d5opt=d5
                endif
              enddo
            enddo
          enddo
        enddo
      enddo
c
      do i=1,ip
        bse(i)=0.d0
      enddo
      do i=ip+1,i1
        bse(i)=d1opt*(dble(i-ip)/dble(id))**2
      enddo
      do i=i1+1,i2
        bse(i)=d1opt+(d2opt-d1opt)*dble(i-i1)/dble(id)
      enddo
      do i=i2+1,i3
        bse(i)=d2opt+(d3opt-d2opt)*dble(i-i2)/dble(id)
      enddo
      do i=i3+1,i4
        bse(i)=d3opt+(d4opt-d3opt)*dble(i-i3)/dble(id)
      enddo
      do i=i4+1,i5
        bse(i)=d4opt+(d5opt-d4opt)*dble(i-i4)/dble(id)
      enddo
      do i=i5+1,i6-1
        bse(i)=d5opt+(d6-d5opt)*dble(i-i5)/dble(id)
      enddo
c
600   continue
c
c     re-smoothing
c
      d1=bse(i6)/dble(i6-ip)
      ks=1+(i6-ip)/2
c
      k=0
800   k=k+1
      do i=ipp,min0(nw,i6+k)
        swp(i)=bse(i)
      enddo
      do i=ipp+1,min0(nw,i6+k)-1
        bse(i)=(swp(i-1)+swp(i)+swp(i+1))/3.d0
      enddo
c
      npeak=0
      do i=ipp+1,i6-1
        if(bse(i).lt.bse(i-1)+d1.and.
     &     bse(i).lt.bse(i+1)-d1.or.
     &     bse(i).gt.bse(i-1)+d1.and.
     &     bse(i).gt.bse(i+1)-d1)npeak=npeak+1
      enddo
      if(k.lt.ks.or.npeak.gt.1)goto 800
c
      do i=1,nw
        vel(i)=vel(i)-bse(i)
      enddo
c
      swp(1)=0.d0
      do i=2,nw
        swp(i)=swp(i-1)+vel(i)*dt
      enddo
      call rampfit(nw,swp,ipp,iw,i1,i2,doffset,sigma,poly)
      sigma=0.d0
      do i=i2,iw+1
        sigma=sigma+dabs(swp(i)-doffset)
      enddo
      sigma=sigma/dble(1+iw-i2)
      delta=0.d0
      do i=ip,iw
        delta=delta+bse(i)*dt
      enddo
      sigma=dmax1(sigma,dabs(delta))
c
      do i=1,nw
        bse(i)=bse(i)+a0+a1*dble(i-1)
      enddo
c
      deallocate(poly,bat,swp)
      return
      end