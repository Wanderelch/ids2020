      subroutine idsscale(isd,smscl,ttscl)
      use idsalloc
      implicit none
      integer*4 isd
      real*8 smscl,ttscl
c
      integer*4 i,j,isf,ism,igns,isar
      real*8 a2,ab,suma2,sumab
      complex*16 ca,cb
c
      if(isd.eq.1)then
c
c       calculate incremental synthetic seismograms
c
        do ism=1,nsm
          do j=1,3
            do i=1,nf
              dsmsyn(i,j,ism)=(0.d0,0.d0)
              do isf=1,nsf
                dsmsyn(i,j,ism)=dsmsyn(i,j,ism)
     &                         +dstf(i,isf)*smgrn(i,j,isf,ism)
              enddo
            enddo
          enddo
        enddo
c
        do ism=1,nsm
          do j=1,3
            dosmsyn(j,ism)=0.d0
            do isf=1,nsf
              dosmsyn(j,ism)=dosmsyn(j,ism)
     &                       +dreal(dstf(1,isf))*osmgrn(j,isf,ism)
            enddo
          enddo
        enddo
c
        do igns=1,ngns
          do j=1,3
            dgnssyn(j,igns)=0.d0
            do isf=1,nsf
              dgnssyn(j,igns)=dgnssyn(j,igns)
     &                       +dreal(dstf(1,isf))*gnsgrn(j,isf,igns)
            enddo
          enddo
        enddo
c
        do isar=1,nsar
          dsarsyn(isar)=0.d0
          do isf=1,nsf
            dsarsyn(isar)=dsarsyn(isar)
     &                   +dreal(dstf(1,isf))*sargrn(isf,isar)
          enddo
        enddo
c
c       scale incremental rupture
c
        suma2=0.d0
        sumab=0.d0
        do ism=1,nsm
          do j=1,3
            if(smwei(j,ism).gt.0.d0)then
              a2=0.d0
              ab=0.d0
              do i=1,nf
                ca=dsmsyn(i,j,ism)
                cb=dsmobs(i,j,ism)
                a2=a2+cdabs(ca)**2
                ab=ab+dreal(ca*dconjg(cb))
              enddo
              suma2=suma2+a2*smwei(j,ism)
              sumab=sumab+ab*smwei(j,ism)
            endif
          enddo
        enddo
c
        smscl=sumab/suma2
c
        do ism=1,nsm
          do j=1,3
            suma2=suma2+dosmsyn(j,ism)**2
     &                 *osmwei(j,ism)*osmsetwei
            sumab=sumab+dosmsyn(j,ism)*dosmobs(j,ism)
     &                 *osmwei(j,ism)*osmsetwei
          enddo
        enddo
        do igns=1,ngns
          do j=1,3
            suma2=suma2+dgnssyn(j,igns)**2
     &                 *gnswei(j,igns)*gnssetwei
            sumab=sumab+dgnssyn(j,igns)*dgnsobs(j,igns)
     &                 *gnswei(j,igns)*gnssetwei
          enddo
        enddo
        do isar=1,nsar
          suma2=suma2+dsarsyn(isar)**2
     &               *sarwei(isar)*sarsetwei
          sumab=sumab+dsarsyn(isar)*dsarobs(isar)
     &               *sarwei(isar)*sarsetwei
        enddo
c
        ttscl=sumab/suma2
      else if(isd.eq.0)then
c
c       calculate cumulative synthetic seismograms
c
        do ism=1,nsm
          do j=1,3
            do i=1,nf
              ssmsyn(i,j,ism)=(0.d0,0.d0)
              do isf=1,nsf
                ssmsyn(i,j,ism)=ssmsyn(i,j,ism)
     &                         +sstf(i,isf)*smgrn(i,j,isf,ism)
              enddo
            enddo
          enddo
        enddo
c
        do ism=1,nsm
          do j=1,3
            sosmsyn(j,ism)=0.d0
            do isf=1,nsf
              sosmsyn(j,ism)=sosmsyn(j,ism)
     &                       +dreal(sstf(1,isf))*osmgrn(j,isf,ism)
            enddo
          enddo
        enddo
c
        do igns=1,ngns
          do j=1,3
            sgnssyn(j,igns)=0.d0
            do isf=1,nsf
              sgnssyn(j,igns)=sgnssyn(j,igns)
     &                       +dreal(sstf(1,isf))*gnsgrn(j,isf,igns)
            enddo
          enddo
        enddo
c
        do isar=1,nsar
          ssarsyn(isar)=0.d0
          do isf=1,nsf
            ssarsyn(isar)=ssarsyn(isar)
     &                   +dreal(sstf(1,isf))*sargrn(isf,isar)
          enddo
        enddo
c
c       scale cumulative rupture
c
        suma2=0.d0
        sumab=0.d0
        do ism=1,nsm
          do j=1,3
            if(smwei(j,ism).gt.0.d0)then
              a2=0.d0
              ab=0.d0
              do i=1,nf
                ca=ssmsyn(i,j,ism)
                cb=ssmobs(i,j,ism)
                a2=a2+cdabs(ca)**2
                ab=ab+dreal(ca*dconjg(cb))
              enddo
              suma2=suma2+a2*smwei(j,ism)
              sumab=sumab+ab*smwei(j,ism)
            endif
          enddo
        enddo
c
        smscl=sumab/suma2
c
        do ism=1,nsm
          do j=1,3
            suma2=suma2+sosmsyn(j,ism)**2
     &                 *osmwei(j,ism)*osmsetwei
            sumab=sumab+sosmsyn(j,ism)*sosmobs(j,ism)
     &                 *osmwei(j,ism)*osmsetwei
          enddo
        enddo
c
        do igns=1,ngns
          do j=1,3
            suma2=suma2+sgnssyn(j,igns)**2
     &                 *gnswei(j,igns)*gnssetwei
            sumab=sumab+sgnssyn(j,igns)*sgnsobs(j,igns)
     &                 *gnswei(j,igns)*gnssetwei
          enddo
        enddo
c
        do isar=1,nsar
          suma2=suma2+ssarsyn(isar)**2
     &               *sarwei(isar)*sarsetwei
          sumab=sumab+ssarsyn(isar)*ssarobs(isar)
     &               *sarwei(isar)*sarsetwei
        enddo
c
        ttscl=sumab/suma2
      endif
c
      return
      end
