      subroutine idsdifgrn(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c     STATIC GREEN'S FUNNCTIONN PARAMETERS
c     ====================================
c
c     Static Green's function source types:
c       1 = strike-slip (m12=m21=1)
c       2 = dip-slip (m13=m31=1)
c       3 = compensated linear vector dipole (CLVD)
c           (m11=m22=-1/2, m33=1) (no tangential component)
c     Static Green's function coordinate system:
c       (z,r,t) = cylindrical with z being downward(!)
c~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      integer*4 unit(3,3)
      character*163 dgrnfile(3,3)
c~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
c     LOCAL WORK SPACES
c     =================
c~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      integer*4 i,n,irdg,izsdg
      integer*4 lend,lenf,istp
      real*8 rdg1,rdg2,drdg,dract,rratio
      real*8 zsdg0,zsdg1,zsdg2,dzsdg,dzact,zratio
      real*8 las,mus,rhos,rhoobs,zobs,laobs,muobs
      real*8 d1,d2,d3,d4
      logical*2 clsh
c
c     OPEN STATIC GREEN'S FUNCTIONS FILES
c     ============================
c
      do lend=len(dgrndir),1,-1
        if(dgrndir(lend:lend).ne.' ')goto 100
      enddo
100   continue
      if(dgrndir(lend:lend).ne.'\\'.or.dgrndir(lend:lend).ne.'/')then
        dgrndir=dgrndir(1:lend)//'/'
        lend=lend+1
      endif
      do i=1,3
        do lenf=len(dgreen(i)),1,-1
          if(dgreen(i)(lenf:lenf).ne.' ')goto 110
        enddo
110     continue
        dgrnfile(i,1)=dgrndir(1:lend)//dgreen(i)(1:lenf)//'.ss'
        dgrnfile(i,2)=dgrndir(1:lend)//dgreen(i)(1:lenf)//'.ds'
        dgrnfile(i,3)=dgrndir(1:lend)//dgreen(i)(1:lenf)//'.cl'
      enddo
c
      do istp=1,3
        do i=1,3
          clsh=istp.eq.3.and.i.eq.3
          if(clsh)goto 300
          unit(i,istp)=10+3*(istp-1)+i
          open(unit(i,istp),file=dgrnfile(i,istp),status='old')
          if(i*istp.eq.1)then
            call skipdoc(unit(i,istp))
            read(unit(i,istp),*)nrdg,rdg1,rdg2,rratio
            if(rdg1.lt.0.d0.or.rdg2.lt.rdg1.or.nrdg.le.1)then
              stop 'Error: wrong no of distance samples!'
            else
              if(istp*i.eq.1)then
                allocate(rdg(nrdg),stat=ierr)
                if(ierr.ne.0)then
                  stop ' Error in idsdifgrn: rdg not allocated!'
                endif
              endif
              if(nrdg.eq.2)then
                drdg=rdg2-rdg1
                rdg(1)=rdg1
                rdg(2)=rdg2
              else
                drdg=2.d0*(rdg2-rdg1)/dble(nrdg-1)/(1.d0+rratio)
                rdg(1)=rdg1
                do irdg=2,nrdg
                  dract=drdg*(1.d0+(rratio-1.d0)*dble(irdg-2)
     &                 /dble(nrdg-2))
                  rdg(irdg)=rdg(irdg-1)+dract
                enddo
              endif
            endif
            call skipdoc(unit(i,istp))
            read(unit(i,istp),*)zobs,laobs,muobs,rhoobs
            call skipdoc(unit(i,istp))
            read(unit(i,istp),*)nzsdg,zsdg1,zsdg2,zratio
            if(zsdg1.le.0.d0.or.zsdg2.lt.zsdg1.or.nzsdg.le.1)then
              stop 'Error: wrong no of depth samples!'
            else
              if(istp*i.eq.1)then
                allocate(zsdg(nzsdg),stat=ierr)
                if(ierr.ne.0)then
                  stop ' Error in idsdifgrn: zsdg not allocated!'
                endif
                allocate(dgrns(nzsdg,nrdg,3,3),stat=ierr)
                if(ierr.ne.0)then
                  stop ' Error in idsdifgrn: dgrns not allocated!'
                endif
              endif
              if(nzsdg.eq.2)then
                dzsdg=zsdg2-zsdg1
                zsdg(1)=zsdg1
                zsdg(2)=zsdg2
              else
                dzsdg=2.d0*(zsdg2-zsdg1)/dble(nzsdg-1)/(1.d0+zratio)
                zsdg(1)=zsdg1
                do izsdg=2,nzsdg
                  dzact=dzsdg*(1.d0+(zratio-1.d0)*dble(izsdg-2)
     &                 /dble(nzsdg-2))
                  zsdg(izsdg)=zsdg(izsdg-1)+dzact
                enddo
              endif
            endif
          else
            call skipdoc(unit(i,istp))
            read(unit(i,istp),*)n,d1,d2,d3
            if(n.ne.nrdg.or.d1.ne.rdg1.or.
     &         d2.ne.rdg2.or.d3.ne.rratio)then
              stop 'Error: different observation sampling in Greens!'
            endif
            call skipdoc(unit(i,istp))
            read(unit(i,istp),*)d1,d2,d3,d4
            if(d1.ne.zobs.or.d2.ne.laobs.or.
     &         d3.ne.muobs.or.d4.ne.rhoobs)then
              stop 'Error: diff. observation site parameters in Greens!'
            endif
            call skipdoc(unit(i,istp))
            read(unit(i,istp),*)n,d1,d2,d3
            if(n.ne.nzsdg.or.d1.ne.zsdg1.or.
     &         d2.ne.zsdg2.or.d3.ne.zratio)then
              stop 'Error: different source sampling in Greens!'
            endif
          endif
300       continue
        enddo
      enddo
c
c     all geodetic Green's function files have been opened
c     ====================================================
c
      do izsdg=1,nzsdg
c
c       read in Green's functions
c
        do istp=1,3
          do i=1,3
            clsh=istp.eq.3.and.i.eq.3
            if(clsh)goto 400
            if(i.eq.1)then
              call skipdoc(unit(i,istp))
              read(unit(i,istp),*)zsdg0,las,mus,rhos
            else
              call skipdoc(unit(i,istp))
              read(unit(i,istp),*)d1,d2,d3,d4
              if(d1.ne.zsdg0.or.d2.ne.las.or.
     &           d3.ne.mus.or.d4.ne.rhos)then
                stop 'Error: different s. layer parameters in greens!'
              endif
            endif
            read(unit(i,istp),*)(dgrns(izsdg,irdg,i,istp),irdg=1,nrdg)
400         continue
          enddo
        enddo
      enddo
c
      do istp=1,3
        do i=1,3
          clsh=istp.eq.3.and.i.eq.3
          if(.not.clsh)close(unit(i,istp))
        enddo
      enddo
c
      return
      end
