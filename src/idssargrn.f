      subroutine idssargrn(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,isf,isar,izsdg,izsdg1,izsdg2,il,nl,ir,ir1,ir2
      real*8 st,di,ra,csst,ssst,csdi,ssdi,dnorth,deast
      real*8 drdg,parea,dwei,ddl,pz1,pz2,dpz,xp,yp,px,py
      real*8 dz1,dz2,xobs,yobs,dis,azi,w1,w2,y1,y2,y3
      real*8 co,si,co2,si2
      real*8 psss,shss,psds,shds,pscl,duz,dur,dut
      real*8 sm(3,3),pmwei(5)
c
c     for calling Okada's subroutine DC3D0
c
      INTEGER*4 IRET
      REAL*4 ALPHA,X,Y,Z,DEPTH,DIPS,
     &       UX,UY,UZ,UXX,UYX,UZX,UXY,UYY,UZY,UXZ,UYZ,UZZ
c
c     more details see Okada's subroutine DC3D
c
      REAL*4 AL1,AL2,AW1,AW2,DISL1,DISL2,DISL3
c
c     allocation of InSAR Green's functions
c
      allocate(sargrn(nsf,nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssargrn: sargrn not allocated!'
c
      Z=0.0
      DISL3=0.0
      ALPHA=sngl(0.5d0/(1.d0-poisson))
c
      drdg=rdg(2)-rdg(1)
c
      do isf=1,nsf
        DEPTH=sngl(sfdep(isf))
        DIPS=sngl(sfdip(isf))
        AL1=sngl(-0.5d0*sflen(isf))
        AL2=sngl(+0.5d0*sflen(isf))
        AW1=sngl(-0.5d0*sfwid(isf))
        AW2=sngl(+0.5d0*sfwid(isf))
c
        st=sfstk(isf)*DEG2RAD
        di=sfdip(isf)*DEG2RAD
        ra=sfrak(isf)*DEG2RAD
c
        csst=dcos(st)
        ssst=dsin(st)
        csdi=dcos(di)
        ssdi=dsin(di)
c
        DISL1=sngl(dcos(ra))
        DISL2=sngl(dsin(ra))
c
c       displacement Green functions
c
        do isar=1,nsar
c
c         transform to Okada's coordinate system
c
          call disazi(REARTH,sflat(isf),sflon(isf),
     &                sarlat(isar),sarlon(isar),dnorth,deast)
          X=sngl(dnorth*csst+deast*ssst)
          Y=sngl(dnorth*ssst-deast*csst)
c
          IRET=1
          call DC3D(ALPHA,X,Y,Z,DEPTH,DIPS,AL1,AL2,AW1,AW2,
     &          DISL1,DISL2,DISL3,UX,UY,UZ,
     &          UXX,UYX,UZX,UXY,UYY,UZY,UXZ,UYZ,UZZ,IRET)
c
c         transform from Okada's to Cartesian system
c
          y1=dble(UX)*ssst-dble(UY)*csst
          y2=dble(UX)*csst+dble(UY)*ssst
          y3=dble(UZ)
          sargrn(isf,isar)=y1*sarcs(1,isar)
     &                    +y2*sarcs(2,isar)
     &                    +y3*sarcs(3,isar)
        enddo
c
c       add differential Green's functions
c
        sm(1,1)=-dsin(di)*dcos(ra)*dsin(2.d0*st)
     &          -dsin(2.d0*di)*dsin(ra)*(dsin(st))**2
        sm(2,2)= dsin(di)*dcos(ra)*sin(2.d0*st)
     &          -dsin(2.d0*di)*dsin(ra)*(dcos(st))**2
        sm(3,3)=-(sm(1,1)+sm(2,2))
        sm(1,2)= dsin(di)*dcos(ra)*dcos(2.d0*st)
     &          +0.5d0*dsin(2.d0*di)*dsin(ra)*dsin(2.d0*st)
        sm(2,1)=sm(1,2)
        sm(1,3)=-dcos(di)*dcos(ra)*dcos(st)
     &          -dcos(2.d0*di)*dsin(ra)*dsin(st)
        sm(3,1)=sm(1,3)
        sm(2,3)=-dcos(di)*dcos(ra)*dsin(st)
     &          +dcos(2.d0*di)*dsin(ra)*dcos(st)
        sm(3,2)=sm(2,3)
c
c       1 = weight for strike-slip: m12=m21=1;
c       2 = weight for dip-slip: m13=m31=1
c       3 = weight for clvd: m33=-m11=-m22=1
c       4 = weight for 45 deg strike-slip: m11=-m22=1
c       5 = weight for 45 deg dip-slip: m23=m32=1
c
        parea=sflen(isf)*sfwid(isf)
c
        pmwei(1)=sm(1,2)*parea
        pmwei(2)=sm(1,3)*parea
        pmwei(3)=sm(3,3)*parea
        pmwei(4)=0.5d0*(sm(1,1)-sm(2,2))*parea
        pmwei(5)=sm(2,3)*parea
c
        pz1=(sfdep(isf)-0.5d0*sfwid(isf)*dsin(di))
        pz2=(sfdep(isf)+0.5d0*sfwid(isf)*dsin(di))
c
        izsdg1=1
        do izsdg=1,nzsdg
          if(zsdg(izsdg).le.pz1)then
            izsdg1=izsdg
          endif
        enddo
c
        izsdg2=nzsdg
        do izsdg=nzsdg,1,-1
          if(zsdg(izsdg).ge.pz2)then
            izsdg2=izsdg
          endif
        enddo
c
        nl=1+idint(sflen(isf)/drdg)
        ddl=sflen(isf)/dble(nl)
c
        do izsdg=izsdg1,izsdg2
          if(izsdg.eq.izsdg1)then
            if(izsdg.lt.nzsdg)then
              dpz=dmax1(0.d0,dmin1(pz2,
     &                  0.5d0*(zsdg(izsdg)+zsdg(izsdg+1)))-pz1)
            else
              dpz=pz2-pz1
            endif
          else if(izsdg.eq.izsdg2)then
            dpz=dmax1(0.d0,pz2-dmax1(pz1,
     &                0.5d0*(zsdg(izsdg)+zsdg(izsdg-1))))
          else
            dz1=zsdg(izsdg)-dmax1(pz1,0.5d0*(zsdg(izsdg)+zsdg(izsdg-1)))
            dz2=dmin1(pz2,0.5d0*(zsdg(izsdg)+zsdg(izsdg+1)))-zsdg(izsdg)
            dpz=dmax1(0.d0,dz1)+dmax1(0.d0,dz2)
          endif
          yp=(zsdg(izsdg)-sfdep(isf))/dsin(di)
          dwei=(ddl*dpz/dsin(di))/parea
          do il=1,nl
            xp=-0.5d0*sfwid(isf)+(dble(il)-0.5d0)*ddl
            px=xp*csst-ssst*yp*csdi
            py=xp*ssst+csst*yp*csdi
c
            do isar=1,nsar
c
c             transform from Aki's to Okada's system
c
              call disazi(REARTH,sflat(isf),sflon(isf),
     &                    sarlat(isar),sarlon(isar),dnorth,deast)
              xobs=dnorth-px
              yobs=deast-py
c
              dis=dsqrt(xobs**2+yobs**2)
              if(dis.gt.0.d0)then
                azi=datan2(yobs,xobs)
              else
                azi=0.d0
              endif
              ir1=0
              do ir=1,nrdg
                if(dis.ge.rdg(ir))then
                  ir1=ir
                endif
              enddo
              ir2=ir1+1
              if(ir1.lt.1.or.ir2.gt.nrdg)then
                stop ' idssargrn: distance range exceeded!'
              endif
c
              w1=dwei*(rdg(ir2)-dis)/(rdg(ir2)-rdg(ir1))
              w2=dwei*(dis-rdg(ir1))/(rdg(ir2)-rdg(ir1))
c
              co=dcos(azi)
              si=dsin(azi)
              co2=dcos(2.d0*azi)
              si2=dsin(2.d0*azi)
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c             pmwei(1-5):
c             1 = weight for strike-slip: m12=m21=1;
c                 poloidal *sin(2*theta), toroidal *cos(2*theta)
c             2 = weight for dip-slip: m13=m31=1
c                 poloidal *cos(theta), toroidal *sin(theta)
c
c             3 = weight for clvd: m33=-m11=-m22=1
c                 axisymmetric
c
c             4 = weight for 45 deg strike-slip: m11=-m22=1
c                 greenfct4(theta) = green1(theta + 45 deg)
c
c             5 = weight for 45 deg dip-slip: m23=m32=1
c                 greenfct5(theta) = green2(theta - 90 deg)
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
              psss=pmwei(1)*si2+pmwei(4)*co2
              shss=pmwei(1)*co2-pmwei(4)*si2
              psds=pmwei(2)*co+pmwei(5)*si
              shds=pmwei(2)*si-pmwei(5)*co
              pscl=pmwei(3)
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c               contributions from the strike-slip components
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
              duz=psss
     &           *(w1*dgrns(izsdg,ir1,1,1)+w2*dgrns(izsdg,ir2,1,1))
              dur=psss
     &           *(w1*dgrns(izsdg,ir1,2,1)+w2*dgrns(izsdg,ir2,2,1))
              dut=shss
     &           *(w1*dgrns(izsdg,ir1,3,1)+w2*dgrns(izsdg,ir2,3,1))
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c             contributions from the dip-slip components
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
              duz=duz+psds
     &           *(w1*dgrns(izsdg,ir1,1,2)+w2*dgrns(izsdg,ir2,1,2))
              dur=dur+psds
     &           *(w1*dgrns(izsdg,ir1,2,2)+w2*dgrns(izsdg,ir2,2,2))
              dut=dut+shds
     &           *(w1*dgrns(izsdg,ir1,3,2)+w2*dgrns(izsdg,ir2,3,2))
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c             contributions from the clvd components
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
              duz=duz+pscl
     &           *(w1*dgrns(izsdg,ir1,1,3)+w2*dgrns(izsdg,ir2,1,3))
              dur=dur+pscl
     &           *(w1*dgrns(izsdg,ir1,2,3)+w2*dgrns(izsdg,ir2,2,3))
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
              y1=dur*si+dut*co
              y2=dur*co-dut*si
              y3=-duz
              sargrn(isf,isar)=sargrn(isf,isar)
     &                        +y1*sarcs(1,isar)
     &                        +y2*sarcs(2,isar)
     &                        +y3*sarcs(3,isar)
            enddo
          enddo
        enddo
      enddo
c
      return
      end
