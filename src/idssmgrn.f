      subroutine idssmgrn(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,j,k,m,k1,k2,ir,ind,is,irsel,ig,igsel
      integer*4 isf,ism,jsm,offset,iglast
      integer*4 i1,i2,itap,ipgrn,ipre,iwin,nwgrn,nwgcut
      integer*4 i3ps(3),i4ps(3)
      real*8 depth,t0,vz,vt,vp,tap
      real*8 ttp,tpp,tsp,tkfp,slwp,tts,tps,tss,tkfs,slws
      real*8 m0,mtt,mpp,mrr,mtp,mpr,mrt,twingrn
      real*8 delta,gamma,fac
      real*8 tshift(3)
c
      real*4 t0r4
      real*8 rmin,dmin,rn,re,z,z1,z2,d,d1,d2
      real*8 expl,clvd,ss12,ss11,ds31,ds23
      real*8 dis,azi,bazi,csa,csb,ssa,ssb,cs2a,ss2a
      real*8 csbhyp,ssbhyp
      real*8 t,a,b,c,e,tpg,ts0,tsg,dtp,dts
c
c     integer*4 fseek
c
      twingrn=dble(ntgrn-1)*dtgrn
c
      allocate(fswap(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: fswap not allocated!'
c
      allocate (vrtp(ntgrn,3),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vrtp not allocated!'
      allocate (vrex(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vrex not allocated!'
      allocate (vtex(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vtex not allocated!'
      allocate (vrss(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vrss not allocated!'
      allocate (vtss(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vtss not allocated!'
      allocate (vpss(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vpss not allocated!'
      allocate (vrds(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vrds not allocated!'
      allocate (vtds(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vtds not allocated!'
      allocate (vpds(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vpds not allocated!'
      allocate (vrcl(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vrcl not allocated!'
      allocate (vtcl(ntgrn),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: vtcl not allocated!'
c
      allocate(bpfg(nf),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: bpfg not allocated!'
c
      call bandpass(nbpfids,fcut1,100.d0*fcut2,df,nf,bpfg)
c
      do i=2,nf
        bpfg(i)=bpfg(i)/dcmplx(0.d0,omi(i))
      enddo
c
      nwgrn=ntgrn+idint(twindow/dtgrn)
      allocate(dsmg(nwgrn,3),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: dsmg not allocated!'
c
      allocate(smgrn(nf,3,nsf,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: smgrn not allocated!'
      allocate(tpsmgrn(nsf,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: tpsmgrn not allocated!'
      allocate(smgrndur(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssmgrn: smgrndur not allocated!'
c
      iglast=0
c
      do isf=1,nsf
        depth=sfdep(isf)
c
c       Green's functions
c
        igsel=1
        dmin=dabs(depth-deps(1))
        do ig=2,ng
          if(dmin.gt.dabs(depth-deps(ig)))then
            igsel=ig
            dmin=dabs(depth-deps(ig))
          endif
        enddo
        do ism=1,nsm
          call disazi(REARTH,smlat(ism),smlon(ism),
     &                       hyplat,hyplon,rn,re)
c
c         bazi = Pi + azimuth (from south to east) of hypocenter relative to station
c
          bazi=PI+datan2(re,-rn)
          ssbhyp=dsin(bazi)
          csbhyp=dcos(bazi)
c
          call disazi(REARTH,sflat(isf),sflon(isf),
     &                       smlat(ism),smlon(ism),rn,re)
c
c         dis = epicentral distance
c         azi = azimuth (from south to east) of receiver relative to source
c
          dis=dsqrt(rn*rn+re*re)
          call idspstt(dis,sfdep(isf),ttp,tkfp,slwp,tts,tkfs,slws)
          tpsmgrn(isf,ism)=ttp
c
          azi=datan2(re,-rn)
          ssa=dsin(azi)
          csa=dcos(azi)
          ss2a=dsin(2.d0*azi)
          cs2a=dcos(2.d0*azi)
c
          call disazi(REARTH,smlat(ism),smlon(ism),
     &                       sflat(isf),sflon(isf),rn,re)
c
c         bazi = Pi + azimuth (from south to east) of subfault relative to station
c
          bazi=PI+datan2(re,-rn)
          ssb=dsin(bazi)
          csb=dcos(bazi)
c
          if(dis.lt.r(1).or.dis.gt.r(nr))then
            stop ' Error in idssmgrn: distance exceeds range!'
          endif
          irsel=1
          rmin=dabs(dis-r(1))
          do ir=2,nr
            if(rmin.gt.dabs(dis-r(ir)))then
              irsel=ir
              rmin=dabs(dis-r(ir))
            endif
          enddo
c
          if(igsel.ne.iglast)then
            if(iglast.gt.0)close(20)
            open(20,file=smgrnfile(igsel),form='unformatted',
     &           status='old')
            iglast=igsel
          endif
          offset=(irsel-1)*(12+(ntgrn*4+8)*10)
          if(offset.gt.0)call fseek(20,offset,0,i)
          read(20)t0r4
          t0=dble(t0r4)
          read(20)(vrex(i),i=1,ntgrn)
          read(20)(vtex(i),i=1,ntgrn)
          read(20)(vrss(i),i=1,ntgrn)
          read(20)(vtss(i),i=1,ntgrn)
          read(20)(vpss(i),i=1,ntgrn)
          read(20)(vrds(i),i=1,ntgrn)
          read(20)(vtds(i),i=1,ntgrn)
          read(20)(vpds(i),i=1,ntgrn)
          read(20)(vrcl(i),i=1,ntgrn)
          read(20)(vtcl(i),i=1,ntgrn)
c
          m0=sflen(isf)*sfwid(isf)*sfmue(isf)
          call moments(m0,sfstk(isf),sfdip(isf),sfrak(isf),
     &                 mtt,mpp,mrr,mtp,mpr,mrt)
c
          expl=(mtt+mpp+mrr)/3.d0
          clvd=mrr-expl
          ss12=mtp
          ss11=(mtt-mpp)/2.d0
          ds31=mrt
          ds23=mpr
c
          do i=1,ntgrn
            vrtp(i,1)=sngl(expl)*vrex(i)+sngl(clvd)*vrcl(i)
     &               +sngl(ss12*ss2a+ss11*cs2a)*vrss(i)
     &               +sngl(ds31*csa+ds23*ssa)*vrds(i)
            vrtp(i,2)=sngl(expl)*vtex(i)+sngl(clvd)*vtcl(i)
     &               +sngl(ss12*ss2a+ss11*cs2a)*vtss(i)
     &               +sngl(ds31*csa+ds23*ssa)*vtds(i)
            vrtp(i,3)=sngl(ss12*cs2a-ss11*ss2a)*vpss(i)
     &               +sngl(ds31*ssa-ds23*csa)*vpds(i)
          enddo
c
          do i=1,ntgrn
c
c           change coordinate system: 1/2/3 = e/n/z
c
            vz=dble(vrtp(i,1))
            vt=dble(vrtp(i,2))
            vp=dble(vrtp(i,3))
            dsmg(i,1)= vt*ssb+vp*csb
            dsmg(i,2)=-vt*csb+vp*ssb
            dsmg(i,3)= vz
          enddo
          do j=1,3
            do i=ntgrn,nwgrn
              dsmg(i,j)=0.d0
            enddo
          enddo
c
c         Green function travel times
c
          tpg=dble(tp(irsel,igsel))
          tsg=dble(ts(irsel,igsel))
c
          ipgrn=idint((tpg-t0)/dtgrn)
c
          if(nfpsv(ism).eq.0)then
            nwgcut=nwgrn
          else
            k=1+idint((tsg-tpg)/dtgrn)
            if(ntgrn-k-ipgrn.lt.idint(trise/dtgrn))then
              stop 'Error in idssmgrn: insufficient GF time window!'
            endif
            nwgcut=nwgrn-k
            do j=1,2
              do i=1,nwgcut
                dsmg(i,j)=dsmg(i+k-1,j)
              enddo
            enddo
            do i=1,nwgcut
              delta=dsmg(i,1)*csbhyp+dsmg(i,2)*ssbhyp
              dsmg(i,1)=delta*csbhyp
              dsmg(i,2)=delta*ssbhyp
            enddo
          endif
c
          itap=min0(ipgrn,1+idint(dmin1(tpre,tpst,ttap)/dtgrn))
c
          i1=max0(1,ipgrn-itap)
          i2=ipgrn
c
          if(nfpsv(ism).eq.0)then
            iwin=1+idint(dmin1(swindow-ttap,tsg-tpg+ttap,
     &                         twingrn-(tpg-t0)-ttap)/dtgrn)
            do j=1,3
              i3ps(j)=min0(ipgrn+iwin,nwgcut-itap)
              i4ps(j)=min0(i3ps(j)+itap,nwgcut)
            enddo
          else
c
c           estimate direct P, pP and sP phases
c
            call depthphase(REARTH,nlayer,hpmod,vpmod,vsmod,deps(igsel),
     &                      dble(tkftp(irsel,igsel)),tpp,tsp,
     &                      dble(tkfts(irsel,igsel)),tps,tss,ierr)
            if(nfpsv(ism).eq.1)then
              delta=dmin1(twingrn-(tpg-t0)-ttap,tsp+ttap,
     &                    tsg-tpg-2.d0*ttap)
              gamma=ttap
            else if(nfpsv(ism).eq.2)then
              delta=dmin1(twingrn-(tpg-t0)-ttap,tpp+ttap,
     &                    tsg-tpg-2.d0*ttap)
              gamma=dmin1(ttap,0.5d0*(tsp-delta))
            else
              delta=dmin1(twingrn-(tpg-t0)-ttap,ttap,
     &                    tsg-tpg-2.d0*ttap)
              gamma=dmin1(ttap,0.25d0*tpp)
            endif
            iwin=1+idint(delta/dtgrn)
c
            i3ps(3)=min0(ipgrn+iwin,nwgcut-itap)
            i4ps(3)=min0(i3ps(3)+1+idint(gamma/dtgrn),nwgcut)
c
            if(nfsh(ism).eq.1)then
              delta=dmin1(twingrn-(tpg-t0)-ttap,tss+ttap,
     &                    tsg-tpg-2.d0*ttap)
              gamma=ttap
            else if(nfsh(ism).eq.2)then
              delta=dmin1(twingrn-(tpg-t0)-ttap,tps+ttap,
     &                    tsg-tpg-2.d0*ttap)
              gamma=dmin1(ttap,0.5d0*(tss-delta))
            else
              delta=dmin1(twingrn-(tpg-t0)-ttap,ttap,
     &                    tsg-tpg-2.d0*ttap)
              gamma=dmin1(ttap,0.25d0*tpp)
            endif
            iwin=1+idint(delta/dtgrn)
c
            do j=1,2
              i3ps(j)=min0(ipgrn+iwin,nwgcut-itap)
              i4ps(j)=min0(i3ps(j)+1+idint(gamma/dtgrn),nwgcut)
            enddo
          endif
c
          if(isf.eq.isfhyp)then
            do j=1,3
              do i=1,i2
                fswap(i)=0.d0
              enddo
              do i=i2+1,i3ps(j)
                fswap(i)=fswap(i-1)+(dsmg(i,j)-dsmg(i-1,j))**2
              enddo
              m=i2
              do i=i2+1,i3ps(j)
                if(fswap(i).le.0.85d0*fswap(i3ps(j)))m=i
              enddo
              smgrndur(j,ism)=dble(m-i2)*dtgrn
            enddo
          endif
c
          do j=1,3
c
c           supress pre- and post-event noise
c
            do i=1,i1
              dsmg(i,j)=0.d0
            enddo
            do i=i1,i2
              dsmg(i,j)=dsmg(i,j)
     &                 *dsin(0.5d0*PI*dble(i-i1)/dble(i2-i1))**2
            enddo
            if(nfpsv(ism).eq.0)then
              do i=i3ps(j),nwgcut
                dsmg(i,j)=dsmg(i,j)
     &                   *dexp(-(dble(i-i3ps(j))*dtgrn/ttap)**2)
              enddo
            else
              do i=i3ps(j),i4ps(j)
                dsmg(i,j)=dsmg(i,j)*dcos(0.5d0*PI*dble(i-i3ps(j))
     &                             /dble(i4ps(j)-i3ps(j)))**2
              enddo
              do i=i4ps(j),nwgcut
                dsmg(i,j)=0.d0
              enddo
            endif
          enddo
c
          do j=1,3
            tshift(j)=tpsmgrn(isf,ism)-t0sm(ism)-tpre
          enddo
          if(nfpsv(ism).gt.0)then
            gamma=tsg-tpg-(tssm(ism)-tpsm(ism))
            do j=1,2
              tshift(j)=tshift(j)+gamma
            enddo
          endif
c
c         synthetics starts at t=tpg-tpre
c
          delta=tpg-t0-tpre
c
c         coverting to displacement and re-sampling from dtgrn (of the original data)
c         to the uniform dt shift the times series so that the P arrival is aligned
c         to the theoretical one
c
          do j=1,3
            dfct(1)=(0.d0,0.d0)
            do i=2,2*nf
              t=dble(i-1)*dt+delta
              k1=1+idint((t-0.5d0*dt)/dtgrn)
              k2=1+idint((t+0.5d0*dt)/dtgrn)
              if(k1.le.1.or.k2.ge.nwgcut)then
                dfct(i)=0.d0
              else
                e=0.d0
                do m=1+k1,k2
                  e=e+dsmg(m,j)
                enddo
                b=dmod(t-0.5d0*dt,dtgrn)/dtgrn
                a=1.d0-b
                d=dmod(t+0.5d0*dt,dtgrn)/dtgrn
                c=1.d0-d
                dfct(i)=(0.5d0*(a*dsmg(k1,j)+b*dsmg(k1+1,j)
     &                 +c*dsmg(k2,j)+d*dsmg(k2+1,j))+e)/dble(1+k2-k1)
              endif
            enddo
c
            do i=1,2*nf
              cfct(i)=dcmplx(dfct(i)*dt,0.d0)
            enddo
c
            call four1w(cfct,dfct,2*nf,-1)
c
c           shift time to get synthetics starting at the same time as the data
c
            do i=1,nf
              b=-omi(i)*tshift(j)
              smgrn(i,j,isf,ism)=cfct(i)*cdexp(dcmplx(0.d0,b))*bpfg(i)
            enddo
          enddo
        enddo
      enddo
c
      close(20)
c
      deallocate(vrtp,vrex,vtex,vrss,vtss,vpss,vrds,vtds,vpds,vrcl,vtcl,
     &           dsmg,fswap)
c
      return
      end