      subroutine idsgetinp(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,ind
c
c     Earthquake location parameters
c
      open(10,file=inputfile,status='old')
      call skipdoc(10)
      read(10,*)year,month,day,hour,minute,hyptime
      call skipdoc(10)
      read(10,*)hyplat,hyplon,hypdep
      hypdep=hypdep*KM2M
      call skipdoc(10)
      read(10,*)mwini
c
c     Waveform data folder
c
      call skipdoc(10)
      read(10,*)smobsdir
      call skipdoc(10)
      read(10,*)stdismin,stdismax
      stdismin=stdismin*KM2M
      stdismax=stdismax*KM2M
      call skipdoc(10)
      read(10,*)osmsetwei
c
c     Waveform Green's function database folder
c
      call skipdoc(10)
      read(10,*)smgrndir
      call skipdoc(10)
      read(10,*)nbpfg,fcut1g,fcut2g
c
c     Static gns displacement data
c
      call skipdoc(10)
      read(10,*)ngns
      if(ngns.gt.0)then
        backspace(10)
        read(10,*)ngns,gnsdatafile
      endif
      call skipdoc(10)
      read(10,*)gnssetwei
      if(ngns.le.0)gnssetwei=0.d0
c
c     Static InSAR LOS displacement data
c
      call skipdoc(10)
      read(10,*)nsar
      if(nsar.gt.0)then
        backspace(10)
        read(10,*)nsar,sardatafile
      endif
      call skipdoc(10)
      read(10,*)sarsetwei
      if(nsar.le.0)sarsetwei=0.d0
c
c     Static Green's function database
c
      call skipdoc(10)
      read(10,*)dgrndir
      call skipdoc(10)
      read(10,*)(dgreen(i),i=1,3)
c
c     Fault parameters
c
      call skipdoc(10)
      read(10,*)idisc
      if(idisc.eq.0)then
        call skipdoc(10)
        read(10,*)finitefault,nsf,iref
      else
        call skipdoc(10)
        read(10,*)strike,dip,rake
      endif
c
c     IDS parameters
c
      call skipdoc(10)
      read(10,*)itermax
c
c     read output parameters
c
      call skipdoc(10)
      read(10,*)outdir
      call skipdoc(10)
      read(10,*)osdir
      call skipdoc(10)
      read(10,*)ebcdir
      call skipdoc(10)
      read(10,*)eqmpfile
      call skipdoc(10)
      read(10,*)estffile
      call skipdoc(10)
      read(10,*)pstffile
      call skipdoc(10)
      read(10,*)rupfrontfile
      call skipdoc(10)
      read(10,*)slpmodel
      call skipdoc(10)
      read(10,*)swfmisfitfile
      call skipdoc(10)
      read(10,*)osmdatafit
      call skipdoc(10)
      read(10,*)gnsdatafit
      call skipdoc(10)
      read(10,*)sardatafit
      call skipdoc(10)
      read(10,*)nsnap
      allocate(snapshot(nsnap),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgetinp: snapshot not allocated!'
      allocate(t1snap(nsnap),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgetinp: t1snap not allocated!'
      allocate(t2snap(nsnap),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsgetinp: t2snap not allocated!'
      do i=1,nsnap
        call skipdoc(10)
        read(10,*)snapshot(i),t1snap(i),t2snap(i)
      enddo
c
      do i=1,120
        if(outdir(i:i).eq.' ')goto 101
      enddo
101   ind=i-1
      if(outdir(ind:ind).eq.'/'.or.outdir(ind:ind).eq.'\')then
        ind=ind-1
        outdir=outdir(1:ind)
      endif
      osdir=outdir(1:ind)//'/'//osdir
      ebcdir=outdir(1:ind)//'/'//ebcdir
      eqmpfile=outdir(1:ind)//'/'//eqmpfile
      estffile=outdir(1:ind)//'/'//estffile
      pstffile=outdir(1:ind)//'/'//pstffile
      rupfrontfile=outdir(1:ind)//'/'//rupfrontfile
      slpmodel=outdir(1:ind)//'/'//slpmodel
      swfmisfitfile=outdir(1:ind)//'/'//swfmisfitfile
      osmdatafit=outdir(1:ind)//'/'//osmdatafit
      gnsdatafit=outdir(1:ind)//'/'//gnsdatafit
      sardatafit=outdir(1:ind)//'/'//sardatafit
      logsmhvce=outdir(1:ind)//'/idslog_hvce.dat'
      logiteration=outdir(1:ind)//'/idslog_iteration.dat'
      do i=1,nsnap
        snapshot(i)=outdir(1:ind)//'/'//snapshot(i)
      enddo
c
c     End of input file
c
      close(10)
      return
      end