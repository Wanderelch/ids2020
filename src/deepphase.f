      subroutine deepphase(nz,h,vp,vs,zs,slwh,tpp,tsp,ierr)
      implicit none
c
c     Input:
c
      integer*4 nz,ierr
      real*8 zs,slwh
      real*8 h(nz),vp(nz),vs(nz)
c
c     Return:
c
      real*8 tpp,tsp
c
      integer*4 i,i0
      real*8 z,z0,slw,slwz,dpp,dsp
c
      ierr=0
      z=0.d0
      do i=1,nz
        z=z+h(i)
        if(z.le.zs)then
          i0=i
          z0=z
        endif
      enddo
c
      tpp=0.d0
      dpp=0.d0
      tsp=0.d0
      dsp=0.d0
      do i=1,i0
        if(i.lt.i0)then
          z=h(i)
        else
          z=zs-z0
        endif
        slw=1.d0/vp(i)
        if(slwh.gt.slw)then
          ierr=1
          return
        endif
        slwz=dsqrt(slw**2-slwh**2)
        tpp=tpp+z*slw**2/slwz
        dpp=dpp+z*slwh/slwz
c
        slw=1.d0/vs(i)
        if(slwh.gt.slw)then
          ierr=2
          return
        endif
        slwz=dsqrt(slw**2-slwh**2)
        tsp=tsp+z*slw**2/slwz
        dsp=dsp+z*slwh/slwz
      enddo
      tsp=tpp+tsp-(dpp+dsp)*slwh
      tpp=2*tpp-2.d0*dpp*slwh
c
      return
      end