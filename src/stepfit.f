      subroutine stepfit(n,y,n1,n2,i0,y0,smin)
      implicit none
      integer*4 n,n1,n2,i0
      real*8 y0,smin
      real*8 y(n)
c
      integer*4 i
      real*8 sigma,ysum
c
      ysum=y(n)
      do i=n-1,n1,-1
        ysum=ysum+y(i)
      enddo
c
      i0=n1
      smin=ysum**2/dble(1+n-n1)
      y0=ysum
c
      do i=n1+1,n2
        sigma=ysum**2/dble(1+n-i)
        ysum=ysum-y(i)
        if(smin.lt.sigma)then
          i0=i
          smin=sigma
          y0=ysum
        endif
      enddo
c
      smin=-smin
      do i=1,n
        smin=smin+y(i)**2
      enddo
      y0=y0/dble(1+n-i0)
c
      return
      end