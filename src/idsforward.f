      subroutine idsforward(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,j,isf,ism,igns,isar,iter,jter,isfc
      real*8 t,slip,moment,fac,slpc,allwei
      real*8 a2,summa,sigma,maxslp,gamma,suma2,sumab,ttscl
      real*8 nrv,smnrv,osmnrv,gnsnrv,sarnrv
      character*80 text
c
      iter=0
      jter=0
      allwei=1.d0+osmsetwei+gnssetwei+sarsetwei
c
      if(ngns.gt.0)then
        sigma=0.d0
        do igns=1,ngns
          do j=1,3
            dgnsobs(j,igns)=sgnsobs(j,igns)
            sgnssyn(j,igns)=0.d0
            dgnssyn(j,igns)=0.d0
            sigma=sigma+sgnsobs(j,igns)**2*gnswei(j,igns)
          enddo
        enddo
        do igns=1,ngns
          do j=1,3
            gnswei(j,igns)=gnswei(j,igns)/sigma
          enddo
        enddo
      endif
c
      if(nsar.gt.0)then
        sigma=0.d0
        do isar=1,nsar
          dsarobs(isar)=ssarobs(isar)
          ssarsyn(isar)=0.d0
          dsarsyn(isar)=0.d0
          sigma=sigma+ssarobs(isar)**2*sarwei(isar)
        enddo
        do isar=1,nsar
          sarwei(isar)=sarwei(isar)/sigma
        enddo
      endif
c
      do ism=1,nsm
        do j=1,3
          do i=1,nf
            ssmsyn(i,j,ism)=(0.d0,0.d0)
          enddo
        enddo
      enddo
c
      do isf=1,nsf
        do i=1,nf
          sstf(i,isf)=(0.d0,0.d0)
        enddo
        do i=1,2*nf
          pstf(i,isf)=0.d0
        enddo
      enddo
c
      open(30,file=pstffile,status='old')
      read(30,'(a6)')text
      itmax=0
      do i=1,2*nf
        read(30,*,end=100)t,(pstf(i,isf),isf=1,nsf)
        itmax=itmax+1
      enddo
100   close(30)
c
      do isf=1,nsf
        fac=sflen(isf)*sfwid(isf)*sfmue(isf)
        do i=1,2*nf
          cfct(i)=dcmplx(pstf(i,isf)/fac,0.d0)
        enddo
c
        call four1w(cfct,dfct(1),2*nf,-1)
c
        do i=1,nf
          sstf(i,isf)=cfct(i)*dcmplx(dt,0.d0)
        enddo
      enddo
c
      do ism=1,nsm
        do j=1,3
          do i=1,nf
            ssmsyn(i,j,ism)=(0.d0,0.d0)
            do isf=1,nsf
              ssmsyn(i,j,ism)=ssmsyn(i,j,ism)
     &                       +sstf(i,isf)*smgrn(i,j,isf,ism)
            enddo
            dsmobs(i,j,ism)=ssmobs(i,j,ism)-ssmsyn(i,j,ism)
          enddo
        enddo
      enddo
c
      slpc=0.d0
      isfc=0
      do isf=1,nsf
        sfslp(isf)=dmax1(0.d0,dreal(sstf(1,isf)))
        if(slpc.lt.sfslp(isf))then
          slpc=sfslp(isf)
          isfc=isf
        endif
      enddo
c
      do ism=1,nsm
        do j=1,3
          sosmsyn(j,ism)=0.d0
          do isf=1,nsf
            sosmsyn(j,ism)=sosmsyn(j,ism)
     &                     +sfslp(isf)*osmgrn(j,isf,ism)
          enddo
          dosmobs(j,ism)=sosmobs(j,ism)-sosmsyn(j,ism)
        enddo
      enddo
c
      do igns=1,ngns
        do j=1,3
          sgnssyn(j,igns)=0.d0
          do isf=1,nsf
            sgnssyn(j,igns)=sgnssyn(j,igns)
     &                     +sfslp(isf)*gnsgrn(j,isf,igns)
          enddo
          dgnsobs(j,igns)=sgnsobs(j,igns)-sgnssyn(j,igns)
        enddo
      enddo
c
      do isar=1,nsar
        ssarsyn(isar)=0.d0
        do isf=1,nsf
          ssarsyn(isar)=ssarsyn(isar)
     &                 +sfslp(isf)*sargrn(isf,isar)
        enddo
        dsarobs(isar)=ssarobs(isar)-ssarsyn(isar)
      enddo
c
c     determine relative weighting of datasets
c
      do ism=1,nsm
        do j=1,3
          smvar(j,ism)=0.d0
          dsmvar(j,ism)=0.d0
          do i=1,nf
            smvar(j,ism)=smvar(j,ism)+cdabs(ssmobs(i,j,ism))**2
            dsmvar(j,ism)=dsmvar(j,ism)+cdabs(dsmobs(i,j,ism))**2
          enddo
          dsmvar(j,ism)=(1.d0-SDWEI)*dsmvar(j,ism)+SDWEI*smvar(j,ism)
          smwei(j,ism)=smwei0(j,ism)/dsmvar(j,ism)
        enddo
      enddo
c
      gamma=0.d0
      do ism=1,nsm
        do j=1,3
          gamma=gamma+smwei(j,ism)*smvar(j,ism)
        enddo
      enddo
c
      do ism=1,nsm
        do j=1,3
          smwei(j,ism)=smwei(j,ism)/gamma
        enddo
      enddo
c
      do ism=1,nsm
        if(nfpsv(ism).eq.0)then
          do j=1,3
            osmwei(j,ism)=smwei0(j,ism)*smvar(j,ism)/dsmvar(j,ism)
          enddo
        else
          do j=1,3
            osmwei(j,ism)=0.d0
          enddo
        endif
      enddo
c
      sigma=0.d0
      do ism=1,nsm
        do j=1,3
          sigma=sigma+sosmobs(j,ism)**2*osmwei(j,ism)
        enddo
      enddo
c
      if(sigma.gt.0.d0)then
        do ism=1,nsm
          do j=1,3
            osmwei(j,ism)=osmwei(j,ism)/sigma
          enddo
        enddo
      endif
c
      maxslp=0.d0
      do i=1,nf
        cfct(i)=(0.d0,0.d0)
      enddo
c
      mw=0.d0
      isfc=0
      do isf=1,nsf
        gamma=sflen(isf)*sfwid(isf)*sfmue(isf)
        sfslp(isf)=dreal(sstf(1,isf))
        mw=mw+sfslp(isf)*gamma
        if(maxslp.lt.sfslp(isf))then
          maxslp=sfslp(isf)
          isfc=isf
        endif
      enddo
c
      mw=(dlog10(mw)-9.1d0)/1.5d0
c
      smnrv=0.d0
      do ism=1,nsm
        do j=1,3
          if(smwei(j,ism).gt.0.d0)then
            a2=0.d0
            do i=1,nf
              a2=a2+cdabs(dsmobs(i,j,ism))**2
            enddo
            smnrv=smnrv+a2*smwei(j,ism)
          endif
        enddo
      enddo
c
      osmnrv=0.d0
      do ism=1,nsm
        do j=1,3
          osmnrv=osmnrv+dosmobs(j,ism)**2*osmwei(j,ism)
        enddo
      enddo
c
      gnsnrv=0.d0
      do igns=1,ngns
        do j=1,3
          gnsnrv=gnsnrv+dgnsobs(j,igns)**2*gnswei(j,igns)
        enddo
      enddo
c
      sarnrv=0.d0
      do isar=1,nsar
        sarnrv=sarnrv+dsarobs(isar)**2*sarwei(isar)
      enddo
c
      nrv=(smnrv+osmnrv*osmsetwei
     &    +gnsnrv*gnssetwei+sarnrv*sarsetwei)/allwei
c
      open(30,file=logiteration,status='unknown')
      write(*,'(a)')' ******************************************'
      write(*,'(a)')'               IDS forward modeling        '
      write(*,'(a)')' ******************************************'
      write(*,'(a)')' (nrv = normalized residual variance)'
      write(*,'(a)')' (swf = seism. waveform, osm = disp. offset from'
     &         //' SM, gns = disp. from GNSS, sar = disp. from InSar)'
      write(*,'(a,$)')' iteration     nrv_all     nrv_swf'
      if(osmsetwei.gt.0.d0)write(*,'(a,$)')'     nrv_osm'
      if(ngns.gt.0)write(*,'(a,$)')'     nrv_gns'
      if(nsar.gt.0)write(*,'(a,$)')'     nrv_sar'
      write(*,'(a)')' smoothing  max_slip  patch_no        Mw'
c
      write(30,'(a)')' ******************************************'
      write(30,'(a)')'               IDS forward modeling        '
      write(30,'(a)')' ******************************************'
      write(30,'(a)')' (nrv = normalized residual variance)'
      write(30,'(a)')' (swf = seism. waveform, osm = disp. offset from'
     &         //' SM, gns = disp. from GNSS, sar = disp. from InSar)'
      write(30,'(a,$)')' iteration     nrv_all     nrv_swf'
      if(osmsetwei.gt.0.d0)write(30,'(a,$)')'     nrv_osm'
      if(ngns.gt.0)write(30,'(a,$)')'     nrv_gns'
      if(nsar.gt.0)write(30,'(a,$)')'     nrv_sar'
      write(30,'(a)')' smoothing  max_slip  patch_no        Mw'
c      
      write(*,'(i10,2f12.6,$)')iter,nrv,smnrv
      if(osmsetwei.gt.0.d0)write(*,'(f12.6,$)')osmnrv
      if(ngns.gt.0)write(*,'(f12.6,$)')gnsnrv
      if(nsar.gt.0)write(*,'(f12.6,$)')sarnrv
      write(*,'(i10,f10.4,i10,f10.4)')jter,maxslp,isfc,mw
c
      write(30,'(i10,2f12.6,$)')iter,nrv,smnrv
      if(osmsetwei.gt.0.d0)write(30,'(f12.6,$)')osmnrv
      if(ngns.gt.0)write(30,'(f12.6,$)')gnsnrv
      if(nsar.gt.0)write(30,'(f12.6,$)')sarnrv
      write(30,'(i10,f10.4,i10,f10.4)')jter,maxslp,isfc,mw
c
      suma2=0.d0
      sumab=0.d0
      do ism=1,nsm
        do j=1,3
          if(smwei(j,ism).gt.0.d0)then
            sigma=0.d0
            gamma=0.d0
            do i=1,nf
              sigma=sigma+cdabs(ssmsyn(i,j,ism))**2
              gamma=gamma+cdabs(ssmobs(i,j,ism))**2
            enddo
            suma2=suma2+sigma**2*smwei(j,ism)
            sumab=sumab+sigma*gamma*smwei(j,ism)
          endif
        enddo
      enddo
      ttscl=sumab/suma2
      mwsm=mw+dlog10(ttscl)/1.5d0
      write(*,'(2(a,f5.2),a)')' moment magnitude: ',mw,
     &      ' (or ',mwsm,' if only fitting power spectra of waveforms)'
      write(*,'(a,f8.2,a,$)')' peak fault slip: ',maxslp,' m,'
      write(*,'(3(a,f8.2),a)')' located at (',
     &            sflat(isfc),' deg_N, ',sflon(isfc),' deg_E, ',
     &            sfdep(isfc)/KM2M,' km)'
      write(30,'(2(a,f4.2),a)')' moment magnitude: ',mw,
     &      ' (or ',mwsm,' if only fitting power spectra of waveforms)'
      write(30,'(a,f6.2,a,$)')' peak fault slip: ',maxslp,' m,'
      write(30,'(3(a,f6.2),a)')' located at (',
     &            sflat(isfc),' deg_N, ',sflon(isfc),' deg_E, ',
     &            sfdep(isfc)/KM2M,' km)'
      close(30)
c
      return
      end