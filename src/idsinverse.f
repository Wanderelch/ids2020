      subroutine idsinverse(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,ism,isf,jsf
      real*8 alfa,beta,sigma,summa,ttp,tkfp,slwp,tts,tkfs,slws
      real*8 re,rn,dis,dis0,azi,azi0,plg,plg0,x,x0,y,y0,z,z0
c
c     define neighboring patches
c
      do isf=1,nsf
        rsmth(isf)=dmax1(0.25d0*vsmean/fcut2,1.5d0*patchsize)
100     i=0
        do jsf=1,nsf
          if(jsf.ne.isf.and.dis3dsf(isf,jsf).le.rsmth(isf))then
            i=i+1
            isfnb(i,isf)=jsf
          endif
        enddo
        if(i.le.0)then
          stop ' Error in idsinverse: isolated fault patch!'
        else if(i.lt.4)then
          rsmth(isf)=rsmth(isf)+patchsize
          goto 100
        endif
        nsfnb(isf)=i
      enddo
c
      allocate(smvar(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsinverse: smvar not allocated!'
      allocate(dsmvar(3,nsm),stat=ierr)
      if(ierr.ne.0)stop ' Error in idsinverse: dsmvar not allocated!'
c
      if(itermax.le.0)then
        call idsforward(ierr)
      else
        write(*,'(a,i4,a)')' initial estimate of rise time <= ',
     &                       idnint(trise),' s'
        write(*,'(a,i4,a)')' signal window of data used <= ',
     &                       idnint(tsource),' s'
        do isf=1,nsf
          dis0=dis3dsf(isfhyp,isf)
          azi0=azisf2hyp(isf)*DEG2RAD
          plg0=plgsf2hyp(isf)*DEG2RAD
          vrpeak=0.d0
          summa=0.d0
          do jsf=1,nsf
            dis=dis3dsf(isfhyp,jsf)
            if(dis.le.dis0)then
              x0=dis*dsin(plg0)*dcos(azi0)
              y0=dis*dsin(plg0)*dsin(azi0)
              z0=dis*dcos(plg0)
              azi=azisf2hyp(jsf)*DEG2RAD
              plg=plgsf2hyp(jsf)*DEG2RAD
              x=dis*dsin(plg)*dcos(azi)
              y=dis*dsin(plg)*dsin(azi)
              z=dis*dcos(plg)
              if(dsqrt((x-x0)**2+(y-y0)**2+(z-z0)**2).le.patchsize)then
                vrpeak=vrpeak+sfvs(jsf)*sflen(jsf)*sfwid(jsf)
                summa=summa+sflen(jsf)*sfwid(jsf)
              endif
            endif
          enddo
c
          vrpeak=vrpeak/summa
c
          alfa=dis3dsf(isfhyp,isf)/(VR2VSLW*vrpeak)
          beta=dis3dsf(isfhyp,isf)/(VR2VSUP*vrpeak)
          if(nearfield.gt.0)then
            sigma=dmax1(HYPOERR/sfvp(isfhyp),alfa-beta)
            it1sf(isf)=1+idint(dmax1(0.d0,alfa-sigma)/dt)
            it2sf(isf)=1+idint(dmin1(alfa+trise,tsource)/dt)
          else
            it1sf(isf)=1+idint(dmax1(0.d0,beta)/dt)
            it2sf(isf)=1+idint(dmin1(beta+trise,tsource)/dt)
          endif
c
          do ism=1,nsm
            call disazi(REARTH,sflat(isf),sflon(isf),
     &                     smlat(ism),smlon(ism),rn,re)
            dis=dsqrt(re*re+rn*rn)
            call idspstt(dis,sfdep(isf),ttp,tkfp,slwp,tts,tkfs,slws)
            it1sf(isf)=max0(it1sf(isf),1+idint((tpsm(ism)-ttp)/dt))
            it2sf(isf)=min0(it2sf(isf),
     &                      1+idint((tsource+tpsm(ism)-ttp)/dt))
            if(dis3dsf(isfhyp,isf).le.HYPOERR)then
              it1sf(isf)=max0(1,it1sf(isf)-
     &                          idint(HYPOERR/sfvp(isfhyp)/dt))
            endif
          enddo
        enddo
        call idskernel(ierr)
      endif
c
      return
      end