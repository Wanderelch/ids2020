      subroutine depthphase(rearth,nz,h,vp,vs,zs,
     &                      tkfp,tpp,tsp,tkfs,tps,tss,ierr)
      implicit none
c
c     estimate depth phases 
c
c     Input:
c
      integer*4 nz,ierr
      real*8 rearth,zs,tkfp,tkfs
      real*8 h(nz),vp(nz),vs(nz)
c
c     Return:
c
      real*8 slwph,slwsh,tpp,tsp,tps,tss
c
c     Local memory
c
      integer*4 i,i0
      real*8 z,z0,rs,ray,rup,rlw,insup,inslw,dpp,dsp,dps,dss,swap
      real*8 deg2rad
c
      real*8, allocatable:: r(:)
c
      allocate(r(nz),stat=ierr)
      if(ierr.ne.0)stop ' Error in depthphase: r not allocated!'
c
      ierr=0
      deg2rad=datan(1.d0)/45.d0
c
      z=0.d0
      r(1)=rearth
      do i=2,nz
        z=z+h(i-1)
        r(i)=rearth-z
        if(z.le.zs)then
          i0=i-1
        endif
      enddo
      if(zs.ge.z)then
        i0=nz
      endif
      rs=rearth-zs
c
      if(tkfp.ge.90.d0)then
        tpp=0.d0
        tsp=0.d0
        goto 101
      endif
c
      slwph=dsin(tkfp*deg2rad)/vp(i0)
      ray=slwph*rs
c
      dpp=0.d0
      tpp=0.d0
      rlw=rs
      do i=i0,1,-1
        rup=r(i)
        swap=ray*vp(i)/rlw
        inslw=dasin(swap)
        insup=dasin(swap*rlw/rup)
        dpp=dpp+inslw-insup
        tpp=tpp+(rup*dcos(insup)-rlw*dcos(inslw))/vp(i)
        rlw=rup
      enddo
      dpp=dpp*rearth
c
      dsp=0.d0
      tsp=0.d0
      rlw=rs
      do i=i0,1,-1
        rup=r(i)
        swap=ray*vs(i)/rlw
        inslw=dasin(swap)
        insup=dasin(swap*rlw/rup)
        dsp=dsp+inslw-insup
        tsp=tsp+(rup*dcos(insup)-rlw*dcos(inslw))/vs(i)
        rlw=rup
      enddo
      dsp=dsp*rearth
c
      tsp=dmax1(0.d0,tpp+tsp-(dpp+dsp)*slwph)
      tpp=dmax1(0.d0,2*tpp-2.d0*dpp*slwph)
c
101   continue
c
      if(tkfs.ge.90.d0)then
        tps=0.d0
        tss=0.d0
        goto 201
      endif
c
      slwsh=dsin(tkfs*deg2rad)/vs(i0)
      ray=slwsh*rs
c
      dss=0.d0
      tss=0.d0
      rlw=rs
      do i=i0,1,-1
        rup=r(i)
        swap=ray*vs(i)/rlw
        inslw=dasin(swap)
        insup=dasin(swap*rlw/rup)
        dss=dss+inslw-insup
        tss=tss+(rup*dcos(insup)-rlw*dcos(inslw))/vs(i)
        rlw=rup
      enddo
      dss=dss*rearth
c
      dps=0.d0
      tps=0.d0
      rlw=rs
      do i=i0,1,-1
        rup=r(i)
        swap=ray*vp(i)/rlw
        if(swap.ge.1.d0)then
          tps=0.d0
          goto 200
        endif
        inslw=dasin(swap)
        insup=dasin(swap*rlw/rup)
        dps=dps+inslw-insup
        tps=tps+(rup*dcos(insup)-rlw*dcos(inslw))/vp(i)
        rlw=rup
      enddo
      dps=dps*rearth
c
      tps=dmax1(0.d0,tss+tps-(dss+dps)*slwsh)
200   continue
      tss=dmax1(0.d0,2*tss-2.d0*dss*slwsh)
201   continue
c
      deallocate(r)
c
      return
      end