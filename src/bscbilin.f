      subroutine bscbilin(n,ip,vel,bserr,dt,offset,sigma)
      implicit none
c
      integer*4 n,ip
      real*8 dt,offset,sigma
      real*8 vel(n),bserr(n)
c
      integer i,j,k,i0,i1,i2,iw,ia,ib,id,ierr
      real*8 a,b,a0,a1,delta,gamma,smin
c
      integer*4 ndeg
      real*8 PI
      data ndeg,PI/1,3.14159265358979d0/
c
      real*8, allocatable:: poly(:,:),bat(:),swap(:)
c
      allocate (swap(n),stat=ierr)
      if(ierr.ne.0)stop ' Error in bscmono: swap not allocated!'
      allocate (poly(n,0:ndeg),stat=ierr)
      if(ierr.ne.0)stop ' Error in bscmono: poly not allocated!'
      allocate (bat(0:ndeg),stat=ierr)
      if(ierr.ne.0)stop ' Error in bscmono: bat not allocated!'
c
c     remove pre-event trend
c
      call d2dfit(ip,vel,poly,bat,1,bserr)
      a0=bserr(1)
      a1=(bserr(ip)-bserr(1))/dble(ip-1)
c
      do i=1,n
        vel(i)=vel(i)-(a0+a1*dble(i-1))
      enddo
c
c     estimate signal duration
c
      swap(1)=0.d0
      do i=2,n
        swap(i)=swap(i-1)+dabs(vel(i)-vel(i-1))
      enddo
      iw=2
      do i=2,n-1
        if(swap(i).le.0.85d0*swap(n))iw=i
      enddo
c
c     empirical bi-linear baseline correction
c
      call d2dfit(1+n-iw,vel(iw),poly,bat,1,bserr(iw))
c
      a=bserr(iw)
      b=(bserr(n)-bserr(iw))/dble(n-iw)
      do i=1,iw-1
        bserr(i)=a+b*dble(i-iw)
      enddo
c
      swap(1)=0.d0
      do i=2,iw-1
        swap(i)=swap(i-1)+vel(i)
      enddo
      do i=iw,n
        swap(i)=swap(i-1)+vel(i)-bserr(i)
      enddo
      call stepfit(n,swap,ip,iw,i0,delta,smin)
      ia=iw-1
      ib=iw
c
      id=max0(1,(iw-ip)/50)
c
      do k=ip,iw-id,id
        do j=max0(k+id,(ip+iw)/2),iw,id
          do i=1,ip-1
            swap(i)=0.d0
          enddo
          do i=ip,k
            swap(i)=swap(i-1)+vel(i)
          enddo
          do i=k+1,j-1
            swap(i)=swap(i-1)+vel(i)-bserr(j)*dble(i-k)/dble(j-k)
          enddo
          do i=j,n
            swap(i)=swap(i-1)+vel(i)-bserr(i)
          enddo
c
          call stepfit(n,swap,ip,iw,i0,delta,gamma)
c
          if(gamma.le.smin)then
            ia=k
            ib=j
            smin=gamma
          endif
        enddo
      enddo
c
      do i=1,ia
        bserr(i)=0.d0
      enddo
      a=bserr(ib)
      do i=ia+1,ib-1
        bserr(i)=a*dble(i-ia)/dble(ib-ia)
      enddo
c
      do i=1,n
        vel(i)=vel(i)-bserr(i)
      enddo
c
      swap(1)=0.d0
      do i=2,n
        swap(i)=swap(i-1)+vel(i)*dt
      enddo
      call rampfit(n,swap,ip,iw,i1,i2,offset,sigma,poly)
      sigma=0.d0
      do i=i2,iw+1
        sigma=sigma+dabs(swap(i)-offset)
      enddo
      sigma=sigma/dble(1+iw-i2)
      delta=0.d0
      do i=ip,i2
        delta=delta+bserr(i)*dt
      enddo
      sigma=dmax1(sigma,dabs(delta))
c
      do i=1,n
        bserr(i)=bserr(i)+a0+a1*dble(i-1)
      enddo
c
      deallocate(swap,poly,bat)
      return
      end