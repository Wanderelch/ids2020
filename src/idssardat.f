      subroutine idssardat(ierr)
      use idsalloc
      implicit none
      integer*4 ierr
c
      integer*4 i,isar
      real*8 sarins,sarazi,sigma
      character*80 header
c
c     allocation of InSAR data
c
      allocate(sarlat(nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssardat: sarlat not allocated!'
      allocate(sarlon(nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssardat: sarlon not allocated!'
      allocate(sarcs(3,nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssardat: sarcs not allocated!'
      allocate(sarwei(nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssardat: sarwei not allocated!'
      allocate(ssarobs(nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssardat: ssarobs not allocated!'
      allocate(dsarobs(nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssardat: dsarobs not allocated!'
      allocate(ssarsyn(nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssardat: ssarsyn not allocated!'
      allocate(dsarsyn(nsar),stat=ierr)
      if(ierr.ne.0)stop ' Error in idssardat: dsarsyn not allocated!'
c
      open(20,file=sardatafile,status='old')
      read(20,'(a1)')header
      sigma=0.d0
      do isar=1,nsar
        read(20,*)sarlat(isar),sarlon(isar),
     &            ssarobs(isar),sarwei(isar),
     &            sarins,sarazi
        sarcs(1,isar)=dsin(sarins*DEG2RAD)*dsin(sarazi*DEG2RAD)
        sarcs(2,isar)=dsin(sarins*DEG2RAD)*dcos(sarazi*DEG2RAD)
        sarcs(3,isar)=dcos(sarins*DEG2RAD)
        sarwei(isar)=1.d0/sarwei(isar)**2
        sigma=sigma+sarwei(isar)
      enddo
      do isar=1,nsar
        sarwei(isar)=sarwei(isar)/sigma
      enddo
      close(20)
      return
      end