      module idsalloc
c===================================================================
c     global constants
c===================================================================
      integer*4 NTMIN,NTMAX,NPATCH
      parameter(NTMIN=1024,NTMAX=2048,NPATCH=800)
      real*8 PI,PI2
      parameter(PI=3.14159265358979d0,PI2=6.28318530717959d0)
      real*8 DEG2RAD,KM2M
      parameter(DEG2RAD=1.745329251994328d-02,KM2M=1.0d+03)
      real*8 REARTH
      parameter(REARTH=6.371d+06)
      real*8 VSREF,VPREF
      parameter(VSREF=3.0d+03,VPREF=dsqrt(3.d0)*VSREF)
      real*8 SDWEI
      parameter(SDWEI=0.1d+00)
      real*8 FCUTLW,FCUTUP
      parameter(FCUTLW=0.02d+00,FCUTUP=0.10d+00)
      real*8 VR2VSLW,VR2VSUP
      parameter(VR2VSLW=0.5d+00,VR2VSUP=0.75d+00)
      real*8 LENGTHMAX,WIDTHMAX
      parameter(LENGTHMAX=600.0d+03,WIDTHMAX=200.0d+03)
      real*8 TPREMIN,TPSTMIN
      parameter(TPREMIN=10.0d+00,TPSTMIN=40.0d+00)
      real*8 HYPOERR
      parameter(HYPOERR=1.5d+04)
      integer*4 nbpfids
      parameter(nbpfids=3)
c===================================================================
c     global variables
c===================================================================
c
c     year,month,day,hour,minute,second = reference time
c
      integer*4 year,month,day,hour,minute
      real*8 second
c
c     nsm = number of waveform stations
c     nsf = number of sub-faults (patches)
c
      integer*4 idisc,iref,nlen,nwid,nsf,nsm,ngns,nsar,nrdg,nzsdg
      real*8 len1,len2,length,wid1,wid2,width,tpre,tpst,ttap
      real*8 reflat,reflon,refdep,strike,dip,rake,mwini
      real*8 poisson,osmsetwei,gnssetwei,sarsetwei
c
c     hypocenter parameters
c
      real*8 hyplat,hyplon,hypdep,hyptime,stdismin,stdismax,mwsm
c
c     shear modulus model
c
      integer*4 nlayer
c
c     IDS parameters
c
      integer*4 nt,nr,ng,ntgrn,nf,itmax,nsnap,isfhyp
      integer*4 itermax,nbpfg,nearfield
      real*8 fcut1,fcut2,fcut1g,fcut2g,swindow,twindow,dt,df
      real*8 patchsize,twgrn,dtgrn,vrpeak,vsmean,mw,trise,tsource
      logical*2 highpass
c
c     output/output folder/file names
c
      character*120 outdir,osdir,ebcdir
      character*120 slpmodel,estffile,pstffile,eqmpfile,rupfrontfile
      character*120 logsmhvce,logiteration
      character*120 dgrndir,dgreen(3)
      character*120 inputfile,smobsdir,smgrndir,finitefault,
     &          gnsobs,osmdatafit,gnsdatafit,sarobs,sardatafit,
     &          earthmodel,tptable,tstable,gnsdatafile,sardatafile,
     &          swfmisfitfile
c
c===================================================================
c     allocatable variables
c===================================================================
c
c     structure model
c
      real*8, allocatable:: hpmod(:)
      real*8, allocatable:: zpmod(:)
      real*8, allocatable:: romod(:)
      real*8, allocatable:: vpmod(:)
      real*8, allocatable:: vsmod(:)
      real*8, allocatable:: mumod(:)
c
      real*4, allocatable:: tkftp(:,:)
      real*4, allocatable:: slwtp(:,:)
      real*4, allocatable:: tp(:,:)
      real*4, allocatable:: tkfts(:,:)
      real*4, allocatable:: slwts(:,:)
      real*4, allocatable:: ts(:,:)
c
c     strong motion network, observed and synthetic waveform data
c
      character*15, allocatable:: smcode(:)
      integer*4, allocatable:: ismtype(:)
      real*8, allocatable:: sampling(:)
      real*8, allocatable:: smlat(:)
      real*8, allocatable:: smlon(:)
      real*8, allocatable:: t0sm(:)
      real*8, allocatable:: twsm(:)
      real*8, allocatable:: epidissm(:)
      real*8, allocatable:: fswap(:)
      real*8, allocatable:: gswap(:)
      real*8, allocatable:: discor(:,:)
      real*8, allocatable:: velcor(:,:)
      real*8, allocatable:: velbserr(:,:)
      real*8, allocatable:: dsmd(:,:)
      real*8, allocatable:: dsmg(:,:)
      complex*16, allocatable:: csmd(:)
      complex*16, allocatable:: csmg(:)
      complex*16, allocatable:: ssmobs(:,:,:)
      complex*16, allocatable:: dsmobs(:,:,:)
      complex*16, allocatable:: ssmsyn(:,:,:)
      complex*16, allocatable:: dsmsyn(:,:,:)
      complex*16, allocatable:: lpfs(:)
      complex*16, allocatable:: bpf0(:)
      complex*16, allocatable:: bpfd(:)
      complex*16, allocatable:: bpfg(:)
c
c     Strong-motion based offsets
c
      real*8, allocatable:: sosmobs(:,:)
      real*8, allocatable:: dosmobs(:,:)
      real*8, allocatable:: sosmsyn(:,:)
      real*8, allocatable:: dosmsyn(:,:)
c
c     gns network, observed and synthetic offset data
c
      real*8, allocatable:: gnslat(:)
      real*8, allocatable:: gnslon(:)
      real*8, allocatable:: sgnsobs(:,:)
      real*8, allocatable:: dgnsobs(:,:)
      real*8, allocatable:: sgnssyn(:,:)
      real*8, allocatable:: dgnssyn(:,:)
c
c     InSAR grid, observed and synthetic offset data
c
      real*8, allocatable:: sarlat(:)
      real*8, allocatable:: sarlon(:)
      real*8, allocatable:: sarcs(:,:)
      real*8, allocatable:: ssarobs(:)
      real*8, allocatable:: dsarobs(:)
      real*8, allocatable:: ssarsyn(:)
      real*8, allocatable:: dsarsyn(:)
c
      real*8, allocatable:: rdg(:)
      real*8, allocatable:: zsdg(:)
      real*8, allocatable:: dgrns(:,:,:,:)
c
c     sub-fault parameters
c
      real*8, allocatable:: sflat(:)
      real*8, allocatable:: sflon(:)
      real*8, allocatable:: sfdep(:)
      real*8, allocatable:: sflen(:)
      real*8, allocatable:: sfwid(:)
      real*8, allocatable:: sfxln(:)
      real*8, allocatable:: sfywd(:)
      real*8, allocatable:: sfmue(:)
      real*8, allocatable:: sfvs(:)
      real*8, allocatable:: sfvp(:)
      real*8, allocatable:: sfro(:)
      real*8, allocatable:: sfstk(:)
      real*8, allocatable:: sfdip(:)
      real*8, allocatable:: sfrak(:)
      real*8, allocatable:: sfslp(:)
      real*8, allocatable:: sfswap(:)
      real*8, allocatable:: sftr(:)
      real*8, allocatable:: estf(:)
      real*8, allocatable:: dfct(:)
      real*8, allocatable:: dswap(:,:)
      real*8, allocatable:: dis3dsf(:,:)
      real*8, allocatable:: azisf2hyp(:)
      real*8, allocatable:: plgsf2hyp(:)
      real*8, allocatable:: pstf(:,:)
      real*8, allocatable:: slpswap(:)
      real*8, allocatable:: rsmth(:)
      real*8, allocatable:: pkspec(:,:,:)
      real*8, allocatable:: clust(:)
      real*8, allocatable:: damping(:)
      complex*16, allocatable:: cfct(:)
      complex*16, allocatable:: csmswap(:)
      complex*16, allocatable:: cswap(:,:)
      complex*16, allocatable:: sstf(:,:)
      complex*16, allocatable:: dstf(:,:)
      complex*16, allocatable:: stfswap(:,:)
      integer*4, allocatable:: isfnb(:,:)
      integer*4, allocatable:: nsfnb(:)
      integer*4, allocatable:: it1sf(:)
      integer*4, allocatable:: it2sf(:)
      integer*4, allocatable:: nfpsv(:),nfsh(:)
      real*8, allocatable:: t1snap(:)
      real*8, allocatable:: t2snap(:)
      real*8, allocatable:: tppsm(:),tspsm(:),tpssm(:),tsssm(:)
      real*8, allocatable:: twincut(:)
      character*80, allocatable:: snapshot(:)
      logical*2, allocatable:: subf(:)
c
c     Green's functions
c
      complex*16, allocatable:: smgrn(:,:,:,:)
      real*8, allocatable:: osmgrn(:,:,:)
      real*8, allocatable:: gnsgrn(:,:,:)
      real*8, allocatable:: sargrn(:,:)
      real*8, allocatable:: r(:)
      real*8, allocatable:: deps(:)
      character*120, allocatable:: smgrnfile(:)
      real*4, allocatable:: vrtp(:,:)
      real*4, allocatable:: vrex(:)
      real*4, allocatable:: vtex(:)
      real*4, allocatable:: vrss(:)
      real*4, allocatable:: vtss(:)
      real*4, allocatable:: vpss(:)
      real*4, allocatable:: vrcl(:)
      real*4, allocatable:: vtcl(:)
      real*4, allocatable:: vrds(:)
      real*4, allocatable:: vtds(:)
      real*4, allocatable:: vpds(:)
c
      real*8, allocatable:: smwei(:,:)
      real*8, allocatable:: smwei0(:,:)
      real*8, allocatable:: smvar(:,:)
      real*8, allocatable:: dsmvar(:,:)
      real*8, allocatable:: osmstd(:,:)
      real*8, allocatable:: osmwei(:,:)
      real*8, allocatable:: mfvar(:,:)
      real*8, allocatable:: gnswei(:,:)
      real*8, allocatable:: sarwei(:)
      real*8, allocatable:: hypdis(:)
      real*8, allocatable:: tpsm(:),tssm(:)
      real*8, allocatable:: smobsdur(:,:)
      real*8, allocatable:: smgrndur(:,:)
      real*8, allocatable:: tpsmsyn(:)
      real*8, allocatable:: tssmsyn(:)
      real*8, allocatable:: psmslw(:)
      real*8, allocatable:: psmtkf(:)
      real*8, allocatable:: smobsazi(:)
      real*8, allocatable:: tpsmgrn(:,:)
      real*8, allocatable:: omi(:)
      end module