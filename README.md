# IDS2020 (packaged as Pyrocko backend)

Code to do kinematic earthquake source imaging based on the Iterative Deconvolution and Stacking (IDS) method using seismic waveform and static displacement jointly. 

IDS has been written by Rongjiang Wang.

Packaging has been done by Malte Metz.


## References

- Zhang, Y., Wang, R., Chen, Y., Xu, L., Du, F., Jin, M., Tu, H. and Dahm, T. (2014), Kinematic
  Rupture Model and Hypocenter Relocation of the 2013 Mw 6.6 Lushan Earthquake Constrained by
  Strong‐Motion and Teleseismic Data, Seismological Research Letters, 85 (1), 15–22,
  doi: https://doi.org/10.1785/0220130126.

- Zhang, Y., Wang, R., and Chen, Y.‐T. (2015), Stability of rapid finite‐fault inversion for the
  2014 Mw6.1 South Napa earthquake, Geophys. Res. Lett., 42, 10, 263–10, 272,
  doi: https://doi.org/10.1002/2015GL066244. 

- Diao, F. , Wang, R. , Aochi, H. , Walter, T.R., Zhang, Y. , Zheng, Y. and Xiong, X. (2016),
  Rapid kinematic finite-fault inversion for an Mw 7+ scenario earthquake in the Marmara Sea: an
  uncertainty study, Geophysical Journal International, 204, 2, 813–824,
  https://doi.org/10.1093/gji/ggv459

- Zheng, X., Zhang, Y., Wang, R., Zhao, L., Li, W., & Huang, Q. (2020),
  Automatic inversions of strong‐motion records for ﬁnite‐fault models of signiﬁcant earthquakes
  in and around Japan. Journal of Geophysical Research: Solid Earth, 125, e2020JB019992.
  https://doi.org/10.1029/2020JB019992

## Compile and install

```
autoreconf -i   # only if 'configure' script is missing
./configure  # optional --prefix=/new/bin/directory/
make
sudo make install
```
